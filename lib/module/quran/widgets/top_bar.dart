import 'package:alquranalkareem/event/change_language_event.dart';
import 'package:alquranalkareem/helper/app_localizations.dart';
import 'package:alquranalkareem/helper/my_event_bus.dart';
import 'package:alquranalkareem/helper/settings_helpers.dart';
import 'package:animations/animations.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import '../../../my_app.dart';
import 'package:alquranalkareem/module/quran/widgets/customAttachedAnimation.dart';
import 'file:///D:/alquranalkareem/lib/module/menu_list.dart';
import 'package:alquranalkareem/module/notes/note_list.dart';
import 'package:alquranalkareem/module/quran/quran_search.dart';
import '../../about_page.dart';

class TopBar extends StatefulWidget {
  @override
  _TopBarState createState() => _TopBarState();
}

class _TopBarState extends State<TopBar> with TickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 800),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Material(
      elevation: 8,
      child: Container(
        height: 122,
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColorLight,
          border: Border.all(width: 1.5, color: Theme.of(context).primaryColor),
        ),
        padding: EdgeInsets.only(top: 5),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              height: 45,
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.only(top: 7, right: 16, left: 16),
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 6,
                    child: Container(
                      height: 45,
                      child: OpenContainer(
                        closedColor: Theme.of(context).hoverColor,

                        closedElevation: 2,
                        openElevation: 8,
                        closedShape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(4)),
                        ),
                        transitionType: ContainerTransitionType.fade,
                        transitionDuration: const Duration(milliseconds: 500),
                        openBuilder: (context, action) {
                          return PickerOpened(
                            () => _controller.forward(),
                            QuranSearch(),
                            Theme.of(context).hoverColor,
                          );
                        },
                        closedBuilder: (context, action) {
                          return PickerCollapsed(
                              Theme.of(context).hoverColor,
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 16),
                                  child: Text(AppLocalizations.of(context).search_hint,
                                      style: TextStyle(
                                          fontFamily: "cairo",
                                          fontWeight: FontWeight.normal,
                                          fontSize: 14,
                                          color:
                                          Theme.of(context).bottomAppBarColor.withOpacity(0.5))),
                                ),
                                Icon(Icons.keyboard_arrow_right,
                                  color: Theme.of(context).bottomAppBarColor,),
                              ],
                            ),
                          );
                        },
                      ),
                    ),
                  ),
//                  IconButton(
//                    icon: Icon(
//                      Icons.notifications_active,
//                      color: Theme.of(context).hoverColor,
//                    ),
//                    onPressed: () {
//                      DatePicker.showTimePicker(context,
//                          locale: LocaleType.ar,
//                          showTitleActions: true, onConfirm: (time) {
//                        print("Time ${time.hour}:${time.minute}");
//                      });
//                    },
//                  ),
                  Expanded(
                    flex: 1,
                    child: IconButton(
                      icon: Icon(
                        FontAwesome.language,
                        color: Theme.of(context).hoverColor,
                      ),
//                    },
                      onPressed: () {
                        BotToast.showAttachedWidget(
                            attachedBuilder: (_) => Padding(
                                  padding: const EdgeInsets.all(16.0),
                                  child: Card(
                                    color: Theme.of(context)
                                        .primaryColor
                                        .withOpacity(0.8),
                                    child: Container(
                                      width: 300,
                                      padding: const EdgeInsets.all(8),
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          Container(
                                            height: 35,
                                            color: Theme.of(context)
                                                .primaryColorDark
                                                .withOpacity(0.7),
                                            child: InkWell(
                                              onTap: () async {
                                                var locale = Locale('en');
                                                await SettingsHelpers.instance
                                                    .setLocale(locale);
                                                Application.changeLocale(
                                                    locale);
                                                MyEventBus.instance.eventBus
                                                    .fire(
                                                  ChangeLanguageEvent()
                                                    ..locale = locale,
                                                );
                                              },
                                              child: FlatButton(
                                                child: Row(
                                                  children: <Widget>[
                                                    Icon(Icons.language,
                                                        size: 15,
                                                        color: Theme.of(context)
                                                            .hoverColor),
                                                    VerticalDivider(),
                                                    Text(
                                                      'English',
                                                      style: TextStyle(
                                                          fontSize: 15,
                                                          color:
                                                              Theme.of(context)
                                                                  .hoverColor),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            height: 35,
                                            color: Theme.of(context)
                                                .primaryColor
                                                .withOpacity(0.5),
                                            child: InkWell(
                                              onTap: () async {
                                                var locale = Locale('ar');
                                                await SettingsHelpers.instance
                                                    .setLocale(locale);
                                                Application.changeLocale(
                                                    locale);
                                                MyEventBus.instance.eventBus
                                                    .fire(
                                                  ChangeLanguageEvent()
                                                    ..locale = locale,
                                                );
                                              },
                                              child: FlatButton(
                                                child: Row(
                                                  children: <Widget>[
                                                    Icon(Icons.language,
                                                        size: 15,
                                                        color: Theme.of(context)
                                                            .hoverColor),
                                                    VerticalDivider(),
                                                    Text(
                                                      'العربية',
                                                      style: TextStyle(
                                                          fontSize: 15,
                                                          color:
                                                              Theme.of(context)
                                                                  .hoverColor),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                            wrapToastAnimation:
                                (controller, cancel, Widget child) =>
                                    CustomAttachedAnimation(
                                      controller: controller,
                                      child: child,
                                    ),
                            animationDuration: Duration(milliseconds: 200),
                            enableSafeArea: true,
                            duration: Duration(seconds: 20),
                            targetContext: context);
                      },
                    ),
                  ),
                ],
              ),
            ),
            Divider(
              height: 12,
            ),
            Container(
              height: 48,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  _btnIcon(AppLocalizations.of(context).menu, Icons.widgets,
                      MenuList()),
                  VerticalDivider(
                    width: 2,
                  ),
                  _btnIcon(AppLocalizations.of(context).notes,
                      MaterialIcons.book, NoteList()),
                  VerticalDivider(
                    width: 2,
                  ),
                  _btnIcon(AppLocalizations.of(context).about_us, Icons.info,
                      AboutPage()),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _btnIcon(String title, IconData iconData, Widget page) {
    return RaisedButton(
      onPressed: () {
        Navigator.push(
            context, MaterialPageRoute(builder: ((context) => page)));
      },
      color: Colors.transparent,
      elevation: 0,
      textColor: Theme.of(context).hoverColor,
      child: Column(
        children: <Widget>[
          Icon(iconData),
          Text(
            title,
            style: TextStyle(
                fontSize: 12, fontFamily: 'cairo', fontWeight: FontWeight.w400),
          )
        ],
      ),
    );
  }
}

class PickerCollapsed extends StatelessWidget {
  Color color;
  Widget wid;

  PickerCollapsed(this.color, this.wid);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 45,
      color: color,
      child: wid,
    );
  }
}

class PickerOpened extends StatelessWidget {
  Function onTap;
  Widget wid;
  Color color;

  PickerOpened(this.onTap, this.wid, this.color);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: color,
      padding: EdgeInsets.only(left: 16, right: 16, top: 40),
      child: wid,
    );
  }
}
