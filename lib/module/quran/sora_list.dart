import 'package:alquranalkareem/helper/app_localizations.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:alquranalkareem/data/model/sorah.dart';
import 'package:alquranalkareem/data/repository/sorah_repository.dart';
import 'package:alquranalkareem/module/quran/show.dart';

class SorahList extends StatefulWidget {
  @override
  _SorahListState createState() => _SorahListState();
}

class _SorahListState extends State<SorahList>
    with AutomaticKeepAliveClientMixin<SorahList> {
  SorahRepository sorahRepository = new SorahRepository();
  List<Sorah> sorahList;

  @override
  void initState() {
    super.initState();
    getList();
  }

  getList() async {
    sorahRepository.all().then((values) {
      setState(() {
        sorahList = values;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Column(
      children: <Widget>[
        Expanded(
          child: Scrollbar(
            child: sorahList != null
                ? ListView.builder(
                    physics: const AlwaysScrollableScrollPhysics(),
                    itemCount: sorahList.length,
                    itemBuilder: (_, index) {
                      Sorah sorah = sorahList[index];
                      return GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              new MaterialPageRoute(
                                  builder: ((context) => QuranShow(
                                    initialPageNum: sorah.pageNum,
                                  ))));
                        },
                        child: Container(
                          height: 50,
                            color: (index % 2 == 0
                                ? Theme.of(context).bottomAppBarColor
                                : Theme.of(context).hoverColor),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 8),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  CircleAvatar(
                                    backgroundImage: AssetImage(
                                      "assets/images/sora_num.png",
                                    ),
                                    radius: 18,
                                    backgroundColor: Colors.transparent,
                                    child: Padding(
                                      padding: const EdgeInsets.only(top: 5.0),
                                      child: Text(
                                        "${sorah.id}",
                                        style: TextStyle(
                                            color: Theme.of(context)
                                                .primaryColorDark,
                                            fontFamily: "maddina",
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                  Text(
                                    AppLocalizations.of(context) == 'en'
                                        ? sorah.nameEn
                                        : sorah.name,
                                    style: TextStyle(
                                        fontFamily: "naskh",
                                        fontWeight: FontWeight.w600,
                                        fontSize: 18,
                                        color: (index % 2 == 0
                                            ? Theme.of(context).hoverColor
                                            : Theme.of(context)
                                                .primaryColorDark)),
                                  ),
                                  RichText(
                                      text: TextSpan(children: [
                                    WidgetSpan(
                                      child: Text(
                                        "${sorah.ayaCount}",
                                        style: TextStyle(
                                            fontFamily: "maddina",
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold,
                                            color: (index % 2 == 0
                                                ? Theme.of(context).hoverColor
                                                : Theme.of(context)
                                                    .primaryColor)),
                                      ),
                                    ),
                                    WidgetSpan(
                                      child: Text(
                                        "${AppLocalizations.of(context).aya_count}: ",
                                        style: TextStyle(
                                            fontFamily: "uthman",
                                            fontSize: 14,
                                            color: (index % 2 == 0
                                                ? Theme.of(context).hoverColor
                                                : Theme.of(context)
                                                    .primaryColor)),
                                      ),
                                    ),
                                  ])),
//                                Text(
//                                  '${AppLocalizations.of(context) == 'ar' ? sorah.nameEn : sorah.name}',
//                                  style: TextStyle(
//                                      color:
//                                          Theme.of(context).primaryColorLight),
//                                ),
                                ],
                              ),
                            )
                            ),
                      );
                    })
                : Text(""),
          ),
        )
      ],
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
