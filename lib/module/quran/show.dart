import 'dart:async';
import 'dart:io';
import 'package:alquranalkareem/data/model/aya.dart';
import 'package:alquranalkareem/data/model/sorah_bookmark.dart';
import 'package:alquranalkareem/data/repository/aya_repository.dart';
import 'package:alquranalkareem/data/repository/sorah_bookmark_repository.dart';
import 'package:alquranalkareem/data/repository/translate2_repository.dart';
import 'package:alquranalkareem/data/repository/translate3_repository.dart';
import 'package:alquranalkareem/data/repository/translate4_repository.dart';
import 'package:alquranalkareem/data/repository/translate5_repository.dart';
import 'package:alquranalkareem/data/repository/translate_repository.dart';
import 'package:alquranalkareem/event/font_size_event.dart';
import 'package:alquranalkareem/helper/app_localizations.dart';
import 'package:alquranalkareem/helper/my_event_bus.dart';
import 'package:alquranalkareem/helper/settings_helpers.dart';
import 'package:alquranalkareem/module/quran/quran_bookmarks.dart';
import 'package:alquranalkareem/module/quran/widgets/bookmark_widget.dart';
import 'package:alquranalkareem/module/quran/widgets/play_card.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_custom_dialog/flutter_custom_dialog.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:preferences/preferences.dart';
import 'package:provider/provider.dart';
import 'package:alquranalkareem/data/model/ayat.dart';
import 'package:alquranalkareem/data/model/sorah.dart';
import 'package:alquranalkareem/data/moor_database.dart';
import 'package:alquranalkareem/data/repository/sorah_repository.dart';
import 'package:alquranalkareem/module/quran/sora_list.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sliding_sheet/sliding_sheet.dart';
import 'package:wakelock/wakelock.dart';
import 'widgets/top_bar.dart';
import 'package:alquranalkareem/module/quran/widgets/dailog_design.dart';


class QuranShow extends StatefulWidget {
  int initialPageNum;
  Bookmark bookmark;
  int sorahNum;

  QuranShow({Key key, this.initialPageNum, this.bookmark, this.sorahNum})
      : super(key: key);

  @override
  _QuranShowState createState() => _QuranShowState();
}

class _QuranShowState extends State<QuranShow>
    with AutomaticKeepAliveClientMixin<QuranShow>, WidgetsBindingObserver {
  static const double maxFontSizeArabic = 40;
  double fontSizeArabic = SettingsHelpers.minFontSizeArabic;
  MyEventBus _myEventBus = MyEventBus.instance;

  StreamSubscription streamEvent;

  SorahRepository sorahRepository = new SorahRepository();
  AyaRepository ayaRepository = new AyaRepository();
  SorahBookmarkRepository sorahBookmarkRepository =
      new SorahBookmarkRepository();
  List<SoraBookmark> soraBookmarkList;
  List<Sorah> sorahList;
  List<Aya> ayaList;
  AppDatabase database;
  PageController _pageController;
  Duration duration;
  Duration position;
  AudioPlayer audioPlayer = new AudioPlayer();
  bool isPlay;
  String currentPlay;
  int currentPage;
  int currentIndex;
  bool isShowControl = true;
  double sliderValue;
  StreamSubscription _durationSubscription;
  StreamSubscription _positionSubscription;
  bool downloading = false;
  String progressString = "0%";
  double progress = 0;
  bool autoPlay = false;
  String translate = '';
  String value;
  double _height = 800;
  double _width = 800;
  double _height2 = 2000;
  double _width2 = 4000;
  String _title;
  Bookmark _editBookmark;
  double _newBookmark;
  bool isPageNeedChange;
  int radioValue = 0;
  int tafIbnkatheer = 1;
  int tafBaghawy = 2;
  int tafQurtubi = 3;
  int tafSaadi = 4;
  int tafTabari = 5;
  var showTaf;
  bool toastShow = false;

  SharedPreferences prefs;

  // saveData() async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   prefs.setDouble('tafSave', 30);
  // }
  //
  // getData() async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   //Return int
  //   double doubleValue = prefs.getDouble('tafSave') ?? 20;
  //   return doubleValue;
  // }

  void handleRadioValueChanged(int val) {
    TranslateRepository2 translateRepository2 = new TranslateRepository2();
    TranslateRepository translateRepository = new TranslateRepository();
    TranslateRepository3 translateRepository3 = new TranslateRepository3();
    TranslateRepository4 translateRepository4 = new TranslateRepository4();
    TranslateRepository5 translateRepository5 = new TranslateRepository5();

    radioValue = val;
    switch (val) {
      case 0:
        showTaf = translateRepository2;
        break;
      case 1:
        showTaf = translateRepository;
        break;
      case 2:
        showTaf = translateRepository3;
        break;
      case 3:
        showTaf = translateRepository4;
        break;
      case 4:
        showTaf = translateRepository5;
        break;
    }
    // setState(() {
    //   SettingsHelpers.instance.tafser(val);
    //   tafser = val;
    //   _myEventBus.eventBus.fire(
    //       FontSizeEvent()..tafserValues = val);
    // });
  }

  void initAudioPlayer() {
    _positionSubscription =
        audioPlayer.onAudioPositionChanged.listen((p) => setState(() {
              position = p;
              //print("posiotn ${p.inMilliseconds}");
            }));
    _durationSubscription = audioPlayer.onDurationChanged.listen((Duration d) {
      if (duration == null) {
        //print("posiotn ${d.inMilliseconds}");
        setState(() => duration = d);
      }
    });
  }

  Future playFile(String url, String fileName) async {
    var path;
    int result;
    try {
      var dir = await getApplicationDocumentsDirectory();
      path = join(dir.path, fileName);
      var file = File(path);
      bool exists = await file.exists();
      if (!exists) {
        try {
          await Directory(dirname(path)).create(recursive: true);
        } catch (e) {
          print(e);
        }
        await downloadFile(path, url, fileName);
      }
      result = await audioPlayer.play(path, isLocal: true);
      if (result == 1) {
        setState(() {
          isPlay = true;
        });
      }
    } catch (e) {
      print(e);
    }
  }

  Future downloadFile(String path, String url, String fileName) async {
    Dio dio = Dio();
    try {
      try {
        await Directory(dirname(path)).create(recursive: true);
      } catch (e) {
        print(e);
      }
      setState(() {
        downloading = true;
        progressString = "0%";
        progress = 0;
      });
      await dio.download(url, path, onReceiveProgress: (rec, total) {
        //print("Rec: $rec , Total: $total");
        setState(() {
          progressString = ((rec / total) * 100).toStringAsFixed(0) + "%";
          progress = (rec / total).toDouble();
        });
      });
    } catch (e) {
      print(e);
    }

    setState(() {
      downloading = false;
      progressString = "100%";
    });
    //print("Download completed");
  }

  showControl() {
    setState(() {
      isShowControl = !isShowControl;
    });
  }

  @override
  void initState() {
    setState(() {
      fontSizeArabic = SettingsHelpers.instance.getFontSizeArabic;
    });
    WidgetsBinding.instance.addObserver(this);
    streamEvent = _myEventBus.eventBus.on<FontSizeEvent>().listen((onData) {
      if (streamEvent != null) {
        setState(() {
          fontSizeArabic = onData.arabicFontSize;
        });
      }
    });
    super.initState();
    initAudioPlayer();
    isPlay = false;
    currentPlay = null;
    isShowControl = false;
    currentPage = widget.initialPageNum;
    currentIndex = widget.initialPageNum - 1;
    _pageController = PageController(initialPage: widget.initialPageNum - 1);
    sliderValue = 0;
    getList();
    _title = null;
    _editBookmark = null;
    _newBookmark = null;
    isPageNeedChange = false;
    toastShow = true;
  }

  _replay(BuildContext context) {
    Navigator.pop(context);
    setState(() {
      isPlay = false;
      currentPlay = null;
    });
    if (widget.sorahNum != null) {
      playSorah();
    } else {
      play(currentPage.toString());
    }
  }

  @override
  void deactivate() {
    _positionSubscription.cancel();
    _durationSubscription.cancel();
    if (isPlay) {
      audioPlayer.pause();
    }
    super.deactivate();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _pageController.dispose();
    audioPlayer.dispose();
    streamEvent?.cancel();
    streamEvent = null;
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      if (isPlay) {
        audioPlayer.pause();
        setState(() {
          isPlay = false;
        });
      }
    }
    //print('state = $state');
  }

  saveLastPlace() async {
    PrefService.setInt("start_page", currentPage);
  }

  newBookmark() async {
    var now = new DateTime.now();
    String lastRead = "${now.year}-${now.month}-${now.day}";
    Bookmark bookmark = new Bookmark(
        id: null, title: _title, pageNum: currentPage, lastRead: lastRead);
    await database.insertBookmark(bookmark);
  }

  saveBookmark2() async {
    var now = new DateTime.now();
    String lastRead = "${now.year}-${now.month}-${now.day}";
    Bookmark bookmark = new Bookmark(
        id: widget.bookmark.id,
        title: widget.bookmark.title,
        pageNum: currentPage,
        lastRead: lastRead);
    await database.updateBookmark(bookmark);
  }

  saveBookmark() async {
    if (_editBookmark == null) {
      newBookmark();
    } else {
      updateBookmark();
    }
  }

  updateBookmark() async {
    Bookmark bookmark = new Bookmark(
        id: _editBookmark.id,
        title: _title,
        pageNum: _editBookmark.pageNum,
        lastRead: _editBookmark.lastRead);
    await database.updateBookmark(bookmark);
    setState(() {
      _editBookmark = null;
    });
  }

  playSorah() async {
    String url;
    String sorahName = "${widget.sorahNum}";
    String fileName;
    if (sorahName.length == 1) {
      sorahName = "00$sorahName";
    } else if (sorahName.length == 2) {
      sorahName = "0$sorahName";
    }
    switch (PrefService.getString('audio_player_sound')) {
      case "Husary_128kbps":
        {
          fileName = "mahmood_khaleel_al-husaree/$sorahName.mp3";
          url = "http://download.quranicaudio.com/quran/$fileName";
        }
        break;
      case "Minshawy_Murattal_128kbps":
        {
          fileName = "muhammad_siddeeq_al-minshaawee/$sorahName.mp3";
          url = "http://download.quranicaudio.com/quran/$fileName";
        }
        break;
    }

    //print("url $url");
    if (isPlay) {
      audioPlayer.pause();
      setState(() {
        isPlay = false;
      });
    } else {
      await playFile(url, fileName);
    }
  }

  play(String page) async {
    if (isPlay) {
      audioPlayer.pause();
      setState(() {
        isPlay = false;
      });
    } else {
      int result;
      if (currentPlay == page) {
        result = await audioPlayer.resume();
        if (result == 1) {
          setState(() {
            isPlay = true;
          });
        }
      } else {
        currentPlay = page;
        String fileName = page;
        if (page.length == 1) {
          fileName = "00$fileName";
        } else if (page.length == 2) {
          fileName = "0$fileName";
        }
        fileName =
            "${PrefService.getString('audio_player_sound')}/PageMp3s/Page$fileName.mp3";
        String url = "http://everyayah.com/data/$fileName";
        await playFile(url, fileName);
      }
    }
    setState(() {
      currentPlay = page;
    });
    audioPlayer.onPlayerCompletion.listen((event) {
      setState(() {
        isPlay = false;
        currentPlay = null;
        position = null;
        duration = null;
        autoPlay = true;
      });
      if (widget.sorahNum == null) {
        _pageController.jumpToPage(currentIndex + 1);
      }
    });
  }

  pageChanged(int index) {
    if (isPlay) audioPlayer.stop();
    print("on Page Changed $index");
    setState(() {
      isPlay = false;
      currentPlay = null;
      currentPage = index + 1;
      currentIndex = index;
      position = null;
      duration = null;
      isShowControl = false;
    });
    if (autoPlay) {
      play(currentPage.toString());
      print("current page $currentPage");
      setState(() {
        autoPlay = false;
      });
    }
    saveLastPlace();
  }

  @override
  Widget build(BuildContext context) {
    YYDialog.init(context);
    database = Provider.of<AppDatabase>(context);
    super.build(context);
    Wakelock.enable();
    return WillPopScope(
      onWillPop: () async {
        audioPlayer.stop();
        setState(() {
          isPlay = false;
          currentPlay = null;
        });
        Wakelock.disable();
        return true;
      },
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          body: OrientationBuilder(
            builder: (context, orientation) {
              return Container(
                child: Stack(
                  children: <Widget>[

                    Directionality(
                        textDirection: TextDirection.rtl,
                        child:
                            Center(child: _quranPages(context, orientation))),
                    Visibility(visible: isShowControl, child: TopBar()),
                    _playCard(context, orientation),
                    _downloadingBar(context),
                    _bottomBar(context, orientation),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: Visibility(
                          visible: isShowControl,
                          child: Stack(
                            children: [
                              Align(
                                  alignment: Alignment.bottomLeft,
                                  child: AnimatedContainerWidget(
                                    'الفاصلة',
                                    QuranBookmarks(),
                                    Icon(
                                      Icons.bookmark,
                                      color: Theme.of(context).hoverColor,
                                    ),
                                    Icon(
                                      Icons.cancel,
                                      color: Theme.of(context).hoverColor,
                                    ),
                                  )),
                              Align(
                                  alignment: Alignment.bottomRight,
                                  child: AnimatedContainerWidget(
                                    'السور',
                                    SorahList(),
                                    Icon(
                                      Icons.list,
                                      color: Theme.of(context).hoverColor,
                                    ),
                                    Icon(
                                      Icons.cancel,
                                      color: Theme.of(context).hoverColor,
                                    ),
                                  )),
                            ],
                          )),
                    ),
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }



  Widget _quranPages(BuildContext context, Orientation orientation) {
    Orientation orientation = MediaQuery.of(context).orientation;
    if (orientation == Orientation.portrait) {
      return Container(
        height: _height,
        width: MediaQuery.of(context).size.width,
        child: PageView.builder(
            controller: _pageController ??
                PageController(
                    initialPage: widget.initialPageNum - 1, keepPage: true),
            itemCount: 604,
            onPageChanged: (page) {
              print("page changed $page");
              pageChanged(page);
            },
            itemBuilder: (_, index) {
              return LayoutBuilder(builder: (context, constrains) {
                if (constrains.maxHeight < 600) {
                  _pageController = PageController(viewportFraction: 1);
                } else if (constrains.maxHeight > 600) {
                  _pageController = PageController(viewportFraction: 1);
                }
                return SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: (index % 2 == 0
                        ? Container(
                            decoration: BoxDecoration(
                                color: Theme.of(context).backgroundColor,
                                borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(12),
                                    bottomRight: Radius.circular(12))),
                            child: _pages(context, index, orientation),
                          )
                        : Container(
                            decoration: BoxDecoration(
                                color: Theme.of(context).backgroundColor,
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(12),
                                    bottomLeft: Radius.circular(12))),
                            child: _pages(context, index, orientation),
                          )));
              });
            }),
      );
    } else {
      return Container(
        height: _height2,
        width: _width2,
        child: PageView.builder(
            controller: _pageController ??
                PageController(
                    initialPage: widget.initialPageNum - 1, keepPage: true),
            itemCount: 604,
            onPageChanged: (page) {
              print("page changed $page");
              pageChanged(page);
            },
            itemBuilder: (_, index) {
              return LayoutBuilder(builder: (context, constrains) {
                if (constrains.maxWidth < 2732) {
                  _pageController = PageController(viewportFraction: 1);
                }
                return SingleChildScrollView(
                  child: (index % 2 == 0
                      ? Container(
                          decoration: BoxDecoration(
                              color: Theme.of(context).backgroundColor,
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(12),
                                  bottomRight: Radius.circular(12))),
                          child: _pages2(context, index, orientation),
                        )
                      : Container(
                          decoration: BoxDecoration(
                              color: Theme.of(context).backgroundColor,
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(12),
                                  bottomLeft: Radius.circular(12))),
                          child: _pages2(context, index, orientation),
                        )),
                );
              });
            }),
      );
    }
  }

  Widget _pages(BuildContext context, int index, Orientation orientation) {
    Orientation orientation = MediaQuery.of(context).orientation;
    return InkWell(
      onLongPress: () {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return _newBookmarkDialog(context, index);
            });
      },
      onDoubleTap: () {
        _height = _height == MediaQuery.of(context).size.height / 1 / 2 * 1.5
            ? MediaQuery.of(context).size.height
            : MediaQuery.of(context).size.height / 1 / 2 * 1.5;
        _width = _width == MediaQuery.of(context).size.width
            ? MediaQuery.of(context).size.width
            : MediaQuery.of(context).size.width;
        setState(() {});
      },
      onTap: () => showControl(),
      child: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 32.0),
          child: Stack(
            children: <Widget>[
              Image.asset(
                "assets/pages/00${index + 1}.png",
                fit: BoxFit.fitHeight,
//                                colorBlendMode: BlendMode.hardLight,
                color: Theme.of(context).brightness == Brightness.light
                    ? null
                    : Colors.white,
                height: orientation == Orientation.portrait
                    ? MediaQuery.of(context).size.height - 160
                    : null,
                width: MediaQuery.of(context).size.width,
                alignment: Alignment.center,
              ),
              Image.asset(
                "assets/pages/000${index + 1}.png",
                fit: BoxFit.fitHeight,
                height: orientation == Orientation.portrait
                    ? MediaQuery.of(context).size.height - 160
                    : null,
                width: MediaQuery.of(context).size.width,
                alignment: Alignment.center,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _pages2(BuildContext context, int index, Orientation orientation) {
    Orientation orientation = MediaQuery.of(context).orientation;
    return InkWell(
      onLongPress: () {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return _newBookmarkDialog(context, index);
            });
      },
      onDoubleTap: () {
        _height2 = _height2 == 2732 ? 9000 : 2732;
        _width2 = _width2 == 2732 ? 9000 : 2732;
        setState(() {});
      },
      onTap: () => showControl(),
      child: Stack(
        children: <Widget>[
          Image.asset(
            "assets/pages/00${index + 1}.png",
            fit: BoxFit.fitHeight,
//                                colorBlendMode: BlendMode.hardLight,
            color: Theme.of(context).brightness == Brightness.light
                ? null
                : Colors.white,
            height: orientation == Orientation.portrait
                ? MediaQuery.of(context).size.height - 160
                : null,
            width: MediaQuery.of(context).size.width,
            alignment: Alignment.center,
          ),
          Image.asset(
            "assets/pages/000${index + 1}.png",
            fit: BoxFit.contain,
            height: orientation == Orientation.portrait
                ? MediaQuery.of(context).size.height - 160
                : null,
            width: MediaQuery.of(context).size.width,
            alignment: Alignment.center,
          ),
        ],
      ),
    );
  }

  Widget _downloadingBar(context) {
    return Visibility(
      visible: downloading,
      child: Align(
        alignment: Alignment.center,
        child: Card(
          color: Theme.of(context).primaryColorDark,
          elevation: 20,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(100),
          ),
          child: Container(
            padding: EdgeInsets.only(top: 8, right: 10, bottom: 8, left: 10),
            child: CircularPercentIndicator(
              radius: 60.0,
              lineWidth: 5.0,
              percent: progress,
              center: new Text(
                progressString,
                style: TextStyle(color: Theme.of(context).hoverColor),
              ),
              progressColor: Theme.of(context).hoverColor,
              backgroundColor: Theme.of(context).bottomAppBarColor,
            ),
          ),
        ),
      ),
    );
  }

  Widget _bottomBar(BuildContext context, Orientation orientation) {
    super.build(context);
    Orientation orientation = MediaQuery.of(context).orientation;
    if (orientation == Orientation.portrait) {
      return Visibility(
        visible: isShowControl,
        child: _slidingSheetPortrait(context, orientation),
      );
    } else {
      return Visibility(
        visible: isShowControl,
        child: _slidingSheetlandscape(context, orientation),
      );
    }
  }

  Widget _playCard(BuildContext context, Orientation orientation) {
    super.build(context);
    Orientation orientation = MediaQuery.of(context).orientation;
    if (orientation == Orientation.portrait) {
      return Align(
        alignment: Alignment(0.00, 0.60),
        child: Visibility(
          visible: isShowControl,
          child: PlayCard(widget: playCardWidget(context)),
        ),
      );
    } else {
      return Visibility(
        visible: isShowControl,
        child: Align(
          alignment: Alignment(-0.70, 0.80),
          child: PlayCard(widget: playCardWidget(context)),
        ),
      );
    }
  }

  Widget playCardWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 35),
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 38),
            child: Directionality(
              textDirection: TextDirection.rtl,
              child: Stack(
                children: [
                  SizedBox(
                    width: 187.0,
                    height: 55.0,
                    child: Center(
                        child: Image.asset('assets/images/card_zakh2.png')),
                  ),
                  Padding(
                    padding: EdgeInsets.only(right: 26, left: 26, top: 4),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        IconButton(
                          icon: Icon(
                            Icons.person,
                            color: Theme.of(context).primaryColorLight,
                          ),
                          onPressed: () {
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return _dialogOptions(context);
                                });
                          },
                        ),
                        Center(
                          child: IconButton(
                            icon: Icon(
                              isPlay
                                  ? Icons.pause_circle_outline
                                  : Icons.play_circle_outline,
                              size: 30,
                            ),
                            color: Theme.of(context).primaryColorLight,
                            onPressed: () {
                              if (widget.sorahNum != null) {
                                playSorah();
                              } else {
                                play("$currentPage");
                              }
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Directionality(
            textDirection: TextDirection.ltr,
            child: Container(
              height: 50,
              child: SliderTheme(
                data: SliderThemeData(
                    thumbShape: RoundSliderThumbShape(enabledThumbRadius: 8)),
                child: Slider(
                  activeColor: Theme.of(context).bottomAppBarColor,
                  inactiveColor: Theme.of(context).primaryColorDark,
                  min: 0,
                  max: duration != null
                      ? duration.inMilliseconds.toDouble()
                      : 0.0,
                  value: position != null
                      ? (position.inMilliseconds.toDouble())
                      : 0.0,
                  onChanged: (value) {
                    audioPlayer.seek(new Duration(milliseconds: value.toInt()));
                    setState(() {
                      sliderValue = value.toDouble();
                    });
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _slidingSheetPortrait(BuildContext context, Orientation orientation) {
    // SoraBookmark soraBookmark = soraBookmarkList[currentPage];
    super.build(context);
    return Container(
      child: SlidingSheet(
        scrollSpec: ScrollSpec.bouncingScroll(),
        color: Theme.of(context).primaryColorLight,
        border: Border.all(width: 1.5, color: Theme.of(context).primaryColor),
        elevation: 8,
        cornerRadius: 12,
        snapSpec: const SnapSpec(
          // Enable snapping. This is true by default.
          snap: true,
          // Set custom snapping points.
          snappings: [85, 120, double.infinity],
          positioning: SnapPositioning.pixelOffset,
        ),
        builder: (context, state) {
          // This is the content of the sheet that will get
          // scrolled, if the content is bigger than the available
          // height of the sheet.
          return Container(
            height: MediaQuery.of(context).size.height / 1 / 2,
            child: _showTafseer(currentPage, orientation),
          );
        },
        headerBuilder: (context, state) {
          return Container(
            height: 30,
            width: double.infinity,
            color: Theme.of(context).primaryColorLight,
            alignment: Alignment.center,
            child: Icon(
              Icons.drag_handle,
              color: Theme.of(context).hoverColor,
              size: 30,
            ),
          );
        },
        footerBuilder: (context, state) {
          SoraBookmark soraBookmark = soraBookmarkList[currentIndex];
          return Container(
            height: 56,
            width: double.infinity,
            color: Theme.of(context).primaryColorLight,
            alignment: Alignment.center,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Container(
                height: MediaQuery.of(context).size.height / 1 / 8,
                width: MediaQuery.of(context).size.width * 0.55,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    color: Theme.of(context).primaryColor),
                child: Stack(
                  children: [
                    Center(
                        child: Image.asset(
                      'assets/images/card_zakh2.png',
                      color: Theme.of(context).accentColor.withOpacity(.7),
                    )),
                    sorahList != null
                        ? Swiper(
                            scrollDirection: Axis.horizontal,
                            control: new SwiperControl(
                                color: Theme.of(context).bottomAppBarColor),
                            physics: const AlwaysScrollableScrollPhysics(),
                            viewportFraction: .6,
                            index: soraBookmark.SoraNum,
                            itemCount: sorahList.length,
                            itemBuilder: (BuildContext context, index) {
                              Sorah sorah = sorahList[index];
                              return Center(
                                child: InkWell(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        new MaterialPageRoute(
                                            builder: ((context) => QuranShow(
                                                  initialPageNum: sorah.pageNum,
                                                ))));
                                  },
                                  child: Text(
                                    '${sorah.name}',
                                    style: TextStyle(
                                        fontFamily: "naskh",
                                        fontWeight: FontWeight.w500,
                                        fontSize: 20,
                                        color: Theme.of(context).accentColor),
                                  ),
                                ),
                              );
                            })
                        : Text(""),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  Widget _slidingSheetlandscape(BuildContext context, Orientation orientation) {
    super.build(context);
    return Container(
      width: MediaQuery.of(context).size.width / 1 / 2,
      child: SlidingSheet(
        scrollSpec: ScrollSpec.overscroll(),
        color: Theme.of(context).primaryColorLight,
        border: Border.all(width: 1.5, color: Theme.of(context).primaryColor),
        elevation: 8,
        cornerRadius: 12,
        snapSpec: const SnapSpec(
          // Enable snapping. This is true by default.
          snap: true,
          // Set custom snapping points.
          snappings: [85, 120, double.infinity],
          positioning: SnapPositioning.pixelOffset,
        ),
        builder: (context, state) {
          // This is the content of the sheet that will get
          // scrolled, if the content is bigger than the available
          // height of the sheet.
          return Container(
            height: MediaQuery.of(context).size.height / 1 / 2 * 1.5,
            child: _showTafseer(currentPage, orientation),
          );
        },
        headerBuilder: (context, state) {
          return Container(
            height: 30,
            width: double.infinity,
            color: Theme.of(context).primaryColorLight,
            alignment: Alignment.center,
            child: Icon(
              Icons.drag_handle,
              color: Theme.of(context).hoverColor,
              size: 30,
            ),
          );
        },
        footerBuilder: (context, state) {
          SoraBookmark soraBookmark = soraBookmarkList[currentIndex];
          return Container(
            height: 56,
            color: Theme.of(context).primaryColorLight,
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Container(
                height: MediaQuery.of(context).size.height / 1 / 8,
                width: MediaQuery.of(context).size.width * 0.35,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    color: Theme.of(context).primaryColor),
                child: Stack(
                  children: [
                    Center(
                        child: Image.asset(
                      'assets/images/card_zakh2.png',
                      color: Theme.of(context).accentColor.withOpacity(.7),
                    )),
                    sorahList != null
                        ? Swiper(
                            scrollDirection: Axis.horizontal,
                            index: soraBookmark.SoraNum,
                            control: new SwiperControl(
                                color: Theme.of(context).bottomAppBarColor),
                            physics: const AlwaysScrollableScrollPhysics(),
                            viewportFraction: .6,
                            itemCount: sorahList.length,
                            itemBuilder: (BuildContext context, index) {
                              Sorah sorah = sorahList[index];
                              return Center(
                                child: InkWell(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        new MaterialPageRoute(
                                            builder: ((context) => QuranShow(
                                                  initialPageNum: sorah.pageNum,
                                                ))));
                                  },
                                  child: Text(
                                    '${sorah.name}',
                                    style: TextStyle(
                                        fontFamily: "naskh",
                                        fontWeight: FontWeight.w500,
                                        fontSize: 20,
                                        color: Theme.of(context).accentColor),
                                  ),
                                ),
                              );
                            })
                        : Text(""),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  getList() async {
    sorahRepository.all().then((values) {
      setState(() {
        sorahList = values;
      });
    });
    sorahBookmarkRepository.all().then((values) {
      setState(() {
        soraBookmarkList = values;
      });
    });
  }

  Widget _dialogOptions(BuildContext context) {
    return StatefulBuilder(builder: (context, setState2) {
      return AlertDialog(
        elevation: 0,
        backgroundColor: Colors.transparent,
        content: DailogDesign(
            AppLocalizations.of(context).select_player,
            ListView(
              children: [
                RadioPreference(
                  AppLocalizations.of(context).reader1,
                  "Husary_128kbps",
                  'audio_player_sound',
                  onSelect: () {
                    _replay(context);
                  },
                ),
                Divider(
                  endIndent: 16,
                  indent: 16,
                  height: 3,
                ),
                RadioPreference(
                  AppLocalizations.of(context).reader2,
                  'Minshawy_Murattal_128kbps',
                  'audio_player_sound',
                  onSelect: () {
                    _replay(context);
                  },
                ),
                Divider(
                  endIndent: 16,
                  indent: 16,
                  height: 3,
                  color: Theme.of(context).bottomAppBarColor,
                ),
              ],
            )),
      );
    });
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;

  Widget _ayaList(int pageNum) {
    TranslateRepository2 translateRepository2 = new TranslateRepository2();
    return FutureBuilder<List<Ayat>>(
        future: showTaf == null
            ? translateRepository2.getPageTranslate(pageNum)
            : showTaf.getPageTranslate(pageNum),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            List<Ayat> ayat = snapshot.data;
            return Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Container(
                  height: 50,
                  decoration: BoxDecoration(
                      color: Theme.of(context).hoverColor,
                      border: Border.symmetric(
                          horizontal: BorderSide(
                              color: Theme.of(context).bottomAppBarColor,
                              width: 2))),
                  child: Center(
                    child: ListView.builder(
                      padding: EdgeInsets.symmetric(horizontal: 15),
                      scrollDirection: Axis.horizontal,
                      itemCount: ayat.length,
                      shrinkWrap: true,
                      itemBuilder: (BuildContext context, position) {
                        Ayat aya = ayat[position];
                        return Container(
                          height: 50,
                          width: 50,
                          child: InkWell(
                            onTap: () {
                              setState(() {
                                translate =
                                    '﴿${aya.ayatext}﴾\n\n${aya.translate}';
                              });
                            },
                            child: Stack(
                              children: <Widget>[
                                Center(
                                  child: Image.asset(
                                    'assets/images/aya3.png',
                                    width: 40,
                                    height: 40,
                                    color: Theme.of(context).brightness ==
                                            Brightness.light
                                        ? null
                                        : Theme.of(context).primaryColorLight,
                                  ),
                                ),
                                Center(
                                  child: Padding(
                                    padding: const EdgeInsets.only(top: 4),
                                    child: Text(
                                      "${aya.ayaNum}",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Theme.of(context).primaryColor,
                                          fontFamily: 'naskh',
                                          fontWeight: FontWeight.w700,
                                          fontSize: 17),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ),
              ],
            );
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        });
  }

  Widget _showTafseer(int pageNum, Orientation orientation) {
    return FutureBuilder<List<Ayat>>(
      builder: (context, snapshot) {
        return Container(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 0.0),
            child: Column(
              children: <Widget>[
                Container(
                  height: 40,
                  color: Theme.of(context).primaryColorLight,
                  child: _changeWidget(pageNum, context, orientation),
                ),
                SizedBox(
                  height: 8,
                ),
                _ayaList(currentPage),
                Flexible(
                  flex: 4,
                  child: Container(
                    height: MediaQuery.of(context).size.height / 1 / 2 * 1.3,
                    width: MediaQuery.of(context).size.width,
                    color: Theme.of(context).primaryColor,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 8, horizontal: 16),
                      child: ConstrainedBox(
                        constraints: BoxConstraints(
                          maxHeight: MediaQuery.of(context).size.height,
                        ),
                        child: Scrollbar(
                          child: SingleChildScrollView(
                            child: Column(
                              children: <Widget>[
                                SelectableText(
                                  "$translate",
                                  toolbarOptions: ToolbarOptions(
                                      copy: true, selectAll: true),
                                  textDirection: TextDirection.rtl,
                                  textAlign: TextAlign.justify,
                                  style: TextStyle(
                                      color: Theme.of(context).hoverColor,
                                      fontFamily: 'naskh',
                                      fontWeight: FontWeight.w100,
                                      fontSize: fontSizeArabic),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Divider()
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _newBookmarkDialog(context, index) {
    SoraBookmark soraBookmark = soraBookmarkList[index];
    _title = soraBookmark.SoraName_ar;
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(8)),
      ),
      backgroundColor: Theme.of(context).hoverColor,
      child: Container(
        height: 132,
        width: MediaQuery.of(context).size.width / 1 / 4,
        decoration: BoxDecoration(
          border:
              Border.all(color: Theme.of(context).bottomAppBarColor, width: 3),
          borderRadius: BorderRadius.all(Radius.circular(8)),
        ),
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: Image.asset(
                  'assets/images/zakh2.png',
                  fit: BoxFit.cover,
                  color: Theme.of(context).bottomAppBarColor.withOpacity(0.3),
                )),
            Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Align(
                  alignment: Alignment.topRight,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      AppLocalizations.of(context).add_new_bookmark,
                      style: TextStyle(
                          color: Theme.of(context).bottomAppBarColor,
                          fontFamily: 'Tajawal',
                          fontWeight: FontWeight.w700,
                          fontSize: 18),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8),
                  child: Container(
                      height: 40,
                      decoration: BoxDecoration(
                          color: Theme.of(context).hoverColor,
                          borderRadius: BorderRadius.all(Radius.circular(8))),
                      child: TextField(
                        textInputAction: TextInputAction.done,
                        onChanged: (value) {
                          setState(() {
                            _title = value.trim();
                          });
                        },
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8),
                                borderSide: BorderSide(
                                    color: Theme.of(context).bottomAppBarColor,
                                    width: 2)),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                  color: Theme.of(context).bottomAppBarColor),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                  color: Theme.of(context).bottomAppBarColor),
                            ),
                            hintText: soraBookmark.SoraName_ar,
                            suffixIcon: const Icon(
                              Icons.bookmark,
                              color: Color(0x99f5410a),
                            ),
                            hintStyle: TextStyle(
                                height: 0.8,
                                color: Theme.of(context)
                                    .primaryColorLight
                                    .withOpacity(0.5),
                                fontFamily: 'cairo',
                                fontSize: 15)),
                      )),
                ),
                SizedBox(
                  height: 8,
                ),
                GestureDetector(
                  child: Align(
                    alignment: Alignment.bottomLeft,
                    child: Container(
                      width: 120,
                      height: 40,
                      decoration: BoxDecoration(
                          color: Theme.of(context).bottomAppBarColor,
                          borderRadius:
                              BorderRadius.only(topRight: Radius.circular(8))),
                      child: Center(
                        child: Text(
                          AppLocalizations.of(context).save,
                          style: TextStyle(
                              color: Theme.of(context).hoverColor,
                              fontFamily: 'Tajawal',
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                    ),
                  ),
                  onTap: () {
                    saveBookmark();
                    Navigator.pop(context);
                  },
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget _changeWidget(
      int pageNum, BuildContext context, Orientation orientation) {
    List<String> tafName = [
      '${AppLocalizations.of(context).tafIbnkatheerN}',
      '${AppLocalizations.of(context).tafBaghawyN}',
      '${AppLocalizations.of(context).tafQurtubiN}',
      '${AppLocalizations.of(context).tafSaadiN}',
      '${AppLocalizations.of(context).tafTabariN}',
    ];
    List<String> tafD = [
      '${AppLocalizations.of(context).tafIbnkatheerD}',
      '${AppLocalizations.of(context).tafBaghawyD}',
      '${AppLocalizations.of(context).tafQurtubiD}',
      '${AppLocalizations.of(context).tafSaadiD}',
      '${AppLocalizations.of(context).tafTabariD}',
    ];
    return Stack(
      children: <Widget>[
        Align(
          alignment: Alignment.centerRight,
          child: InkWell(
            onTap: () {
              return showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      elevation: 0,
                      backgroundColor: Colors.transparent,
                      content: DailogDesign(
                        AppLocalizations.of(context).tafChange,
                        StatefulBuilder(builder:
                            (BuildContext context, StateSetter setState) {
                          return ListView.builder(
                            shrinkWrap: true,
                            itemCount: 5,
                            itemBuilder: (BuildContext context, int index) {
                              return Column(
                                children: <Widget>[
                                  RadioListTile(
                                    value: index,
                                    groupValue: radioValue,
                                    title: Text(
                                      tafName[index],
                                      style: TextStyle(
                                          fontSize: 16.0,
                                          fontFamily: 'Tajawal',
                                          fontWeight: FontWeight.w400,
                                          color: Theme.of(context).hoverColor),
                                    ),
                                    subtitle: Text(
                                      tafD[index],
                                      style: TextStyle(
                                          fontSize: 10.0,
                                          fontFamily: 'Tajawal',
                                          fontWeight: FontWeight.normal,
                                          color: Theme.of(context)
                                              .hoverColor
                                              .withOpacity(.5)),
                                    ),
                                    onChanged: (val) {
                                      setState(() {
                                        handleRadioValueChanged(val);
                                      });
                                    },
                                  ),
                                  Divider(
                                    endIndent: 16,
                                    indent: 16,
                                    height: 3,
                                  )
                                ],
                              );
                            },
                          );
                        }),
                      ),
                    );
                  });
            },
            child: Container(
              width: 100,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment(-0.1, 0.0),
                    end: Alignment.centerRight,
                    colors: [
                      Theme.of(context).hoverColor,
                      Theme.of(context).primaryColorLight,
                    ]),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8),
                    bottomLeft: Radius.circular(8)),
                boxShadow: [
                  BoxShadow(
                    color: Theme.of(context).hoverColor,
                    offset: Offset(0.0, 1.0), //(x,y)
                    blurRadius: 5.0,
                  ),
                ],
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Icon(
                    FontAwesome.book,
                    size: 24,
                    color: Theme.of(context).primaryColorLight,
                  ),
                ),
              ),
            ),
          ),
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: InkWell(
            onTap: () {
              return YYDialog().build()
                ..width = 250
                ..borderRadius = 8.0
                ..gravity = Gravity.center
                ..backgroundColor =
                    Theme.of(context).primaryColor.withOpacity(0.8)
                ..margin = EdgeInsets.only(top: 80, right: 20)
                ..widget(
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      StatefulBuilder(builder:
                          (BuildContext context, StateSetter setState) {
                        return Slider(
                          min: SettingsHelpers.minFontSizeArabic,
                          max: maxFontSizeArabic,
                          value: fontSizeArabic,
                          label: '$fontSizeArabic',
                          activeColor: Theme.of(context).hoverColor,
                          inactiveColor: Theme.of(context).bottomAppBarColor,
                          onChanged: (double value) {
                            setState(() {
                              SettingsHelpers.instance.fontSizeArabic(value);
                              fontSizeArabic = value;
                              _myEventBus.eventBus.fire(
                                  FontSizeEvent()..arabicFontSize = value);
                            });
                          },
                        );
                      }),
                      Container(
                        decoration: BoxDecoration(
                            color: Theme.of(context).hoverColor,
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(5.0),
                              topRight: Radius.circular(5.0),
                              bottomLeft: Radius.circular(5.0),
                              bottomRight: Radius.circular(5.0),
                            )),
                        alignment: Alignment.center,
                        child: Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Text('${fontSizeArabic.toInt()}',
                              style: TextStyle(
                                  fontSize: 20,
                                  color: Theme.of(context).primaryColorLight)),
                        ),
                      ),
                    ],
                  ),
                )
                ..show();
            },
            child: Container(
              width: 100,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment(-0.1, 0.0),
                    colors: [
                      Theme.of(context).primaryColorLight,
                      Theme.of(context).hoverColor,
                    ]),
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(8),
                    bottomRight: Radius.circular(8)),
                boxShadow: [
                  BoxShadow(
                    color: Theme.of(context).hoverColor,
                    offset: Offset(0.0, 1.0), //(x,y)
                    blurRadius: 5.0,
                  ),
                ],
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Icon(
                    Icons.format_size,
                    size: 24,
                    color: Theme.of(context).primaryColorLight,
                  ),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}
