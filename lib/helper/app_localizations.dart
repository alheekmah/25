import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:alquranalkareem/l10n/messages_all.dart';
import 'package:preferences/preference_service.dart';
import 'package:quiver/strings.dart';

// Run this flutter packages pub run intl_translation:extract_to_arb --output-dir=lib/l10n lib/localizations/app_localizations.dart
// Copy base arb (intl_messages.arb) to each language
// Run this flutter packages pub run intl_translation:generate_from_arb --output-dir=lib/l10n \ --no-use-deferred-loading lib/localizations/app_localizations.dart lib/l10n/intl_*.arb
// Run above command every time we change arb file

class AppLocalizations {
  static Future<AppLocalizations> load(Locale locale) {
    final String name =
    isBlank(locale?.countryCode) ? locale.languageCode : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);

    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      return AppLocalizations();
    });
  }

  static AppLocalizations of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }


  String lang(){
    return PrefService.getString("locale");
  }

  String get appName {
    return Intl.message(
      'Alquran Alkareem',
      name: 'appName',
      desc: 'appName',
    );
  }

  String get search_hint {
    return Intl.message(
      "Search the verses of the Quran",
      name: 'search_hint',
      desc: 'search_hint',
    );
  }

  String get menu {
    return Intl.message(
      "Menu",
      name: 'menu',
      desc: 'menu',
    );
  }

  String get notes {
    return Intl.message(
      'Notes',
      name: 'notes',
      desc: 'notes',
    );
  }

  String get note_title {
    return Intl.message(
      'Note Title',
      name: 'note_title',
      desc: 'note_title',
    );
  }

  String get add_new_note {
    return Intl.message(
      'Add a new note',
      name: 'add_new_note',
      desc: 'add_new_note',
    );
  }

  String get note_details {
    return Intl.message(
      'Note Details',
      name: 'note_details',
    );
  }

  String get bookmarks {
    return Intl.message(
      'Bookmarks Saved',
      name: 'bookmarks',
    );
  }

  String get bookmark_title {
    return Intl.message(
      'Bookmark Name',
      name: 'bookmark_title',
    );
  }

  String get add_new_bookmark {
    return Intl.message(
      'Add a new Bookmark',
      name: 'add_new_bookmark',
    );
  }

  String get save {
    return Intl.message(
      'Save',
      name: 'save',
    );
  }

  String get edit {
    return Intl.message(
      'Edit',
      name: 'edit',
    );
  }

  String get azkar {
    return Intl.message(
      'Fortress of the Muslim',
      name: 'azkar',
    );
  }

  String get qibla {
    return Intl.message(
      'qibla direction',
      name: 'qibla',
    );
  }

  String get salat {
    return Intl.message(
      'Prayer Times',
      name: 'salat',
    );
  }

  String get aya_count {
    return Intl.message(
      'Number of verses',
      name: 'aya_count',
    );
  }

  String get quran_sorah {
    return Intl.message(
      'All Sorah',
      name: 'quran_sorah',
    );
  }

  String get about_us {
    return Intl.message(
      'About App',
      name: 'about_us',
    );
  }

  String get stop_title {
    return Intl.message(
      'The noble Qur’an - Al-Heekmah Library',
      name: 'stop_title',
    );
  }

  String get about_app {
    return Intl.message(
      "The application of the Qur’an Al Kareem is characterized by its approval of the edition of the King Fahd Complex for Printing the Noble Qur’an in Madinah because of its reliability and mastery.\nThe Quran Al Kareem application is sponsored by the \"Al-Heekmah Library\".\nIt is a well-designed application that allows you to read interactively with the text of the electronic Quran, listen to recitations, study the  Quran Al Kareem and memorize it with ease.",
      name: 'about_app',
    );
  }

  String get about_app2 {
    return Intl.message(
      'Among the most important features of the application :',
      name: 'about_app2',
    );
  }

  String get about_app3 {
    return Intl.message(
      "◉ Reading in portrait and landscape mode.\n◉ The application facilitates the text search feature in the verses of the Qur'an through the instant search and display of results with pages in addition to the ability to go to the page.\n◉ Save reading sites so that the reader can save the page and return to it whenever he wants.\n◉ Add notes.\n◉ Listen to the sura of the Qur’an with the reading of the two sheikhs, \"Mahmoud Khalil Al-Hosary, Muhammad Siddiq Al-Minshawi.\".\n◉ The application provides tafseer for each verse.\n◉ Change the interpretation.\n◉ Change the font size of the tafsir.\n◉ Sura list.\n◉ Easily navigate between the sura.\n◉ The application allows reading the rulings of recitations.\n◉ The application features the addition of a complete Hisn Al Muslim and divided according to the adhkar so that the reader can easily navigate between sections.\n◉ Add any section to favorites.\n◉ The application provides the reader with night reading, which stains the background in black and white lines, to give the reader complete comfort when reading in low light environments.",
      name: 'about_app3',
    );
  }

  String get email {
    return Intl.message(
      'Library communication account :\nalheekmahlib@gmail.com',
      name: 'email',
    );
  }

  String get select_player {
    return Intl.message("Reader selected",
      name: 'select_player',
    );
  }

  String get delete {
    return Intl.message("Delete",
      name: 'delete',
    );
  }

  String get page {
    return Intl.message("Page",
      name: 'page',
    );
  }

  String get search_word {
    return Intl.message("Ayah To search",
      name: 'search_word',
    );
  }

  String get search_description {
    return Intl.message("You can search for all verses of the Noble Qur’an, just type a word from the verse.",
      name: 'search_description',
    );
  }

  String get fontSize {
    return Intl.message("Change Font Size",
      name: 'fontSize',
    );
  }

  String get waqfName {
    return Intl.message("Stop Signs",
      name: 'waqfName',
    );
  }

  String get onboardTitle1 {
    return Intl.message("Easy interface",
      name: 'onboardTitle1',
    );
  }

  String get onboardDesc1 {
    return Intl.message("- Ease of searching for a verse. \n- Change the language. \n- Listen to the page. \n- Change the reader.",
    );
  }

  String get onboardTitle2 {
    return Intl.message("Show The Tafseer",
      name: 'onboardTitle1',
    );
  }

  String get onboardDesc2 {
    return Intl.message("The interpretation of each verse can be read by pulling the list up.",
    );
  }

  String get onboardTitle3 {
    return Intl.message("Click Options",
      name: 'onboardTitle1',
    );
  }

  String get onboardDesc3 {
    return Intl.message("1- When you double click the page is enlarged.\n2- Upon long click you will be presented with the option to save the page.\n3- When you press once, the menus appear.",
    );
  }

  String get light {
    return Intl.message("Light",
      name: 'light',
    );
  }

  String get dark {
    return Intl.message("Dark",
      name: 'dark',
    );
  }

  String get azkarfav {
    return Intl.message("Athkar Favorite",
      name: 'azkarfav',
    );
  }

  String get themeTitle {
    return Intl.message("Night reading",
      name: 'themeTitle',
    );
  }

  String get sorah {
    return Intl.message("Sorah",
      name: 'sorah',
    );
  }

  String get part {
    return Intl.message("Part",
      name: 'part',
    );
  }

  String get langChange {
    return Intl.message("Change the language",
      name: 'langChange',
    );
  }

  String get tafChange {
    return Intl.message("Choose an tafseer",
      name: 'tafChange',
    );
  }

  String get tafIbnkatheerN {
    return Intl.message("Tafsir Ibn Kathir",
      name: 'tafIbnkatheerN',
    );
  }

  String get tafBaghawyN {
    return Intl.message("Tafsir al-Baghawi",
      name: 'tafBaghawyN',
    );
  }

  String get tafQurtubiN {
    return Intl.message("Tafsir al-Qurtubi",
      name: 'tafQurtubiN',
    );
  }

  String get tafSaadiN {
    return Intl.message("Tafsir As-Sadi",
      name: 'tafSaadiN',
    );
  }

  String get tafTabariN {
    return Intl.message("Tafsir al-Tabari",
      name: 'tafTabariN',
    );
  }

  String get tafIbnkatheerD {
    return Intl.message("Tafsir al-Quran al-Azim",
      name: 'tafIbnkatheerD',
    );
  }

  String get tafBaghawyD {
    return Intl.message("Maalim al-Tanzil",
      name: 'tafBaghawyD',
    );
  }

  String get tafQurtubiD {
    return Intl.message("Al-Jami li Ahkam al-Quran",
      name: 'tafQurtubiD',
    );
  }

  String get tafSaadiD {
    return Intl.message("Taiseer Kalam Almannan",
      name: 'tafSaadiD',
    );
  }

  String get tafTabariD {
    return Intl.message("Jami al-Bayan an Tawil [ay] al Quran",
      name: 'tafTabariD',
    );
  }

  String get appLang {
    return Intl.message("App Language",
      name: 'appLang',
    );
  }

  String get setting {
    return Intl.message("Setting",
      name: 'setting',
    );
  }

  String get reader1 {
    return Intl.message("Mahmoud Khalil Al-Hussary",
      name: 'reader1',
    );
  }

  String get reader2 {
    return Intl.message("Mohamed Siddiq El-Minshawi",
      name: 'reader2',
    );
  }

  String get alheekmahlib {
    return Intl.message("Alheekmah Library",
      name: 'alheekmahlib',
    );
  }

  String get backTo {
    return Intl.message("Back To",
      name: 'backTo',
    );
  }

  String get next {
    return Intl.message("Next",
      name: 'next',
    );
  }

  String get start {
    return Intl.message("Start",
      name: 'start',
    );
  }
}

class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  Locale locale;
  List<Locale> supportedLocales;

  AppLocalizationsDelegate({
    this.locale,
    this.supportedLocales,
  });

  @override
  bool isSupported(Locale locale) =>
      supportedLocales.map((v) => v.languageCode).contains(locale.languageCode);

// This does not change
  @override
  Future<AppLocalizations> load(Locale locale) => AppLocalizations.load(locale);

  @override
  bool shouldReload(AppLocalizationsDelegate old) => true;
}
