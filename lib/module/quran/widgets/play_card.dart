import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';

class PlayCard extends StatelessWidget {
  Widget widget;
  Duration duration;
  Duration position;
  AudioPlayer audioPlayer = new AudioPlayer();
  bool isPlay;
  String currentPlay;
  int currentPage;
  int currentIndex;

  PlayCard({
    this.widget,
  });
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 14,
      color: Colors.transparent,
      child: Container(
        height: 165,
        width: 250,
        child: Stack(
          children: [
            Image.asset('assets/images/card_zakh.png',
            color: Theme.of(context).brightness == Brightness.light
                ? null
                : Theme.of(context).hoverColor.withOpacity(.9),),
            widget
          ],
        )
        // Stack(
        //   children: <Widget>[
        //     Padding(
        //       padding: const EdgeInsets.all(8.0),
        //       child: Container(
        //           child: widget,
        //           decoration: BoxDecoration(
        //             color: Theme.of(context).hoverColor,
        //             borderRadius: BorderRadius.only(
        //               topLeft: Radius.circular(80),
        //               topRight: Radius.circular(80)
        //             ),
        //           )),
        //     ),
        //   ],
        // ),
      ),
    );
  }
}
