import 'package:alquranalkareem/custom_clipper/drawClip.dart';
import 'package:alquranalkareem/helper/app_localizations.dart';
import 'package:alquranalkareem/list_screen/quran_juz.dart';
import 'package:alquranalkareem/module/quran/widgets/share.dart';
import 'package:alquranalkareem/themes/custom_theme.dart';
import 'package:alquranalkareem/themes/themes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MenuPage extends StatefulWidget {
  @override
  _MenuPageState createState() => _MenuPageState();
}

class _MenuPageState extends State<MenuPage>
    with SingleTickerProviderStateMixin {
  void _changeTheme(BuildContext buildContext, MyThemeKeys key) {
    CustomTheme.instanceOf(buildContext).changeTheme(key);
  }

  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      value: 0.0,
      duration: Duration(seconds: 25),
      upperBound: 1,
      lowerBound: -1,
      vsync: this,
    )..repeat();
  }

  @override
  void dispose() {
    _controller.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final share = Provider.of<Share>(context);
    var size = MediaQuery.of(context).size;
    return ListView(
      children: <Widget>[
        Stack(
          alignment: Alignment.topCenter,
          children: [
            AnimatedBuilder(
              animation: _controller,
              builder: (BuildContext context, Widget child) {
                return ClipPath(
                  clipper: DrawClip(_controller.value),
                  child: Container(
                    height: size.height * 0.3,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                          begin: Alignment.bottomLeft,
                          end: Alignment.topRight,
                          colors: [
                            Theme.of(context).primaryColor,
                            Theme.of(context).bottomAppBarColor
                          ]),
                    ),
                  ),
                );
              },
            ),
            Container(
              padding: EdgeInsets.only(top: 90),
              child: Text(
                AppLocalizations.of(context).appName,
                style: TextStyle(
                    color: Theme.of(context).hoverColor,
                    fontSize: 35,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Tajawal'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 126),
              child: Text(
                AppLocalizations.of(context).alheekmahlib,
                style: TextStyle(
                    color: Theme.of(context).hoverColor, fontFamily: 'Tajawal'),
              ),
            ),
            GestureDetector(
              child: Align(
                alignment: Alignment.bottomRight,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    height: 40,
                    width: 110,
                    decoration: BoxDecoration(
                        color:
                            Theme.of(context).primaryColorLight.withOpacity(.6),
                        borderRadius: BorderRadius.all(Radius.circular(8))),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            AppLocalizations.of(context).backTo,
                            style: TextStyle(
                                color: Theme.of(context).hoverColor,
                                fontSize: 14,
                                fontFamily: 'Tajawal'),
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Image.asset(
                            'assets/images/splash_icon.png',
                            scale: 16,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: GestureDetector(
            child: Container(
              height: 120,
              decoration: BoxDecoration(
                color: Theme.of(context).bottomAppBarColor,
                borderRadius: BorderRadius.all(Radius.circular(4)),
              ),
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Image.asset(
                    'assets/images/juz.png',
                    scale: 10.0,
                    color: Theme.of(context).hoverColor,
                  ),
                  Image.asset(
                    'assets/images/juz.png',
                    scale: 1.0,
                    color: Theme.of(context).primaryColorLight.withOpacity(.1),
                  ),
                ],
              ),
            ),
            onTap: () {
              Navigator.push(context,
                  new MaterialPageRoute(builder: ((context) => QuranJuz())));
            },
          ),
        ),
        // SizedBox(
        //   height: 24,
        // ),
        Padding(
          padding: const EdgeInsets.only(top: 24),
          child: Column(
            children: [
              Container(
                height: 20,
                width: MediaQuery.of(context).size.width,
                decoration:
                    BoxDecoration(color: Theme.of(context).bottomAppBarColor),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Text(
                    AppLocalizations.of(context).themeTitle,
                    textAlign: TextAlign.start,
                    style: TextStyle(
                        color: Theme.of(context).hoverColor,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Tajawal'),
                  ),
                ),
              ),
              Divider(
                color: Theme.of(context).primaryColorDark,
                height: 7,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Container(
                    height: 190,
                    width: 100,
                    child: Card(
                      color: Theme.of(context).bottomAppBarColor,
                      child: InkWell(
                        child: Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Stack(
                            alignment: Alignment.bottomCenter,
                            children: <Widget>[
                              Stack(
                                alignment: Alignment.center,
                                children: <Widget>[
                                  Center(
                                    child: Image.asset(
                                      'assets/images/light.jpg',
//                                          scale: 20,
                                    ),
                                  ),
                                  Container(
                                      width: 100,
                                      height: 100,
                                      child: Icon(Icons.done,
                                          size: 70,
                                          color: Theme.of(context).brightness ==
                                                  Brightness.light
                                              ? Color(0xffcdba72)
                                              : Colors.transparent)),
                                ],
                              ),
                              Container(
                                height: 30,
                                alignment: Alignment.center,
                                width: MediaQuery.of(context).size.width,
                                color: Theme.of(context).primaryColorLight,
                                child: Text(AppLocalizations.of(context).light,
                                    style: TextStyle(
                                        fontFamily: 'cairo',
                                        fontSize: 16,
                                        color: Theme.of(context).hoverColor)),
                              ),
                            ],
                          ),
                        ),
                        onTap: () {
                          _changeTheme(context, MyThemeKeys.LIGHT);
                        },
                      ),
                    ),
                  ),
                  Container(
                    height: 190,
                    width: 100,
                    child: Card(
                      color: Theme.of(context).bottomAppBarColor,
                      child: InkWell(
                        child: Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Stack(
                            alignment: Alignment.bottomCenter,
                            children: <Widget>[
                              Stack(
                                alignment: Alignment.center,
                                children: <Widget>[
                                  Center(
                                    child: Image.asset(
                                      'assets/images/dark.jpg',
                                    ),
                                  ),
                                  Container(
                                      width: 100,
                                      height: 100,
                                      child: Icon(Icons.done,
                                          size: 70,
                                          color: Theme.of(context).brightness ==
                                                  Brightness.dark
                                              ? Color(0xffcdba72)
                                              : Colors.transparent)),
                                ],
                              ),
                              Container(
                                height: 30,
                                alignment: Alignment.center,
                                width: MediaQuery.of(context).size.width,
                                color: Theme.of(context).primaryColorLight,
                                child: Text(AppLocalizations.of(context).dark,
                                    style: TextStyle(
                                        fontFamily: 'cairo',
                                        fontSize: 16,
                                        color: Theme.of(context).hoverColor)),
                              ),
                            ],
                          ),
                        ),
                        onTap: () {
                          _changeTheme(context, MyThemeKeys.DARK);
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        SizedBox(
          height: MediaQuery.of(context).size.height / 1 / 40,
        ),
        Align(
          alignment: Alignment.bottomRight,
          child: Container(
            width: 50,
            height: 50,
            decoration: BoxDecoration(
                color: Theme.of(context).primaryColorLight,
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(8),
                    topLeft: Radius.circular(8))),
            child: Center(
                child: IconButton(
              icon: Icon(
                Icons.share,
                color: Theme.of(context).hoverColor,
              ),
              onPressed: () {
                share.share();
              },
            )),
          ),
        )
      ],
    );
  }
}
