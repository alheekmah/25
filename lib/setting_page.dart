import 'package:flutter/material.dart';
import 'package:preferences/preferences.dart';
import 'custom_clipper/topWaveClipper.dart';
import 'event/change_language_event.dart';
import 'helper/app_localizations.dart';
import 'helper/my_event_bus.dart';
import 'helper/settings_helpers.dart';
import 'my_app.dart';

class SettingPage extends StatefulWidget {
  SettingPage() : super();
  @override
  _SettingPageState createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        backgroundColor: Theme.of(context).hoverColor,
        body: SingleChildScrollView(
          child: Scrollbar(
            child: Container(
              height: MediaQuery.of(context).size.height,
              child: Stack(children: [
                ClipPath(
                  clipper: TopWaveClipper(),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Theme.of(context).primaryColorLight,
                    ),
                    height: MediaQuery.of(context).size.height / 2.5,
                  ),
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 28),
                    child: IconButton(
                      icon: Icon(
                        Icons.arrow_back,
                        color: Theme.of(context).primaryColorLight,
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 64, vertical: 170),
                    child: Text(
                      AppLocalizations.of(context).setting,
                      style: TextStyle(
                        color: Theme.of(context).primaryColorLight,
                        fontSize: 40,
                        fontFamily: 'Tajawal',
                      ),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    height: 50,
                    decoration: BoxDecoration(
                      color:
                          Theme.of(context).bottomAppBarColor.withOpacity(.4),
                    ),
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    SizedBox(
                      height: 300,
                    ),
                    Container(
                      color: Theme.of(context).bottomAppBarColor,
                      child: Column(
                        children: [
                          Text(
                            AppLocalizations.of(context).appLang,
                            style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 18.0,
                                fontFamily: 'Tajawal',
                                color: Theme.of(context).hoverColor),
                          ),
                          Container(
                              child: Column(
                            children: [
                              Container(
                                height: 35,
                                color: Theme.of(context)
                                    .primaryColorLight
                                    .withOpacity(0.2),
                                child: InkWell(
                                  onTap: () async {
                                    var locale = Locale('en');
                                    await SettingsHelpers.instance
                                        .setLocale(locale);
                                    Application.changeLocale(locale);
                                    MyEventBus.instance.eventBus.fire(
                                      ChangeLanguageEvent()..locale = locale,
                                    );
                                  },
                                  child: FlatButton(
                                    child: Row(
                                      children: <Widget>[
                                        Icon(Icons.language,
                                            size: 15,
                                            color: Theme.of(context)
                                                .backgroundColor),
                                        VerticalDivider(),
                                        Text(
                                          'English',
                                          style: TextStyle(
                                              fontSize: 15,
                                              color: Theme.of(context)
                                                  .backgroundColor),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                height: 35,
                                color: Theme.of(context)
                                    .primaryColorLight
                                    .withOpacity(0.3),
                                child: InkWell(
                                  onTap: () async {
                                    var locale = Locale('ar');
                                    await SettingsHelpers.instance
                                        .setLocale(locale);
                                    Application.changeLocale(locale);
                                    MyEventBus.instance.eventBus.fire(
                                      ChangeLanguageEvent()..locale = locale,
                                    );
                                  },
                                  child: FlatButton(
                                    child: Row(
                                      children: <Widget>[
                                        Icon(Icons.language,
                                            size: 15,
                                            color: Theme.of(context)
                                                .backgroundColor),
                                        VerticalDivider(),
                                        Text(
                                          'العربية',
                                          style: TextStyle(
                                              fontSize: 15,
                                              color: Theme.of(context)
                                                  .backgroundColor),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          )),
                        ],
                      ),
                    ),
                    Divider(),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      color: Theme.of(context).bottomAppBarColor,
                      child: Column(
                        children: [
                          Text(
                            AppLocalizations.of(context).select_player,
                            style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 18.0,
                                fontFamily: 'Tajawal',
                                color: Theme.of(context).hoverColor),
                          ),
                          Container(
                            height: 50,
                            color: Theme.of(context)
                                .primaryColorLight
                                .withOpacity(0.2),
                            child: RadioPreference(
                              AppLocalizations.of(context).reader1,
                              'Husary_128kbps',
                              'audio_player_sound',
                            ),
                          ),
                          Container(
                            height: 50,
                            color: Theme.of(context)
                                .primaryColorLight
                                .withOpacity(0.3),
                            child: RadioPreference(
                              AppLocalizations.of(context).reader2,
                              'Minshawy_Murattal_128kbps',
                              'audio_player_sound',
                            ),
                          ),
                        ],
                      ),
                    ),
                    Divider(),
                    SizedBox(
                      height: 20,
                    ),
                    GestureDetector(
                      child: Align(
                        alignment: Alignment.bottomLeft,
                        child: Container(
                          width: 160,
                          height: 50,
                          decoration: BoxDecoration(
                              color: Theme.of(context).primaryColorLight,
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(8))),
                          child: Center(
                            child: Text(
                              AppLocalizations.of(context).save,
                              style: TextStyle(
                                  color: Theme.of(context).hoverColor,
                                  fontFamily: 'Tajawal',
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                        ),
                      ),
                      onTap: () {
                        Navigator.of(context).pushNamed("/HomeScreen");
                      },
                    ),
                  ],
                ),
              ]),
            ),
          ),
        ));
  }
}
