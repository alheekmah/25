import 'package:alquranalkareem/helper/app_localizations.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:alquranalkareem/data/model/aya.dart';
import 'package:alquranalkareem/data/repository/aya_repository.dart';
import 'package:alquranalkareem/module/quran/show.dart';
import 'package:flutter_icons/flutter_icons.dart';

class QuranSearch extends StatefulWidget {
  Function onSubmitted;

  @override
  _QuranSearchState createState() => _QuranSearchState();
}

class _QuranSearchState extends State<QuranSearch> {

  AyaRepository ayaRepository = new AyaRepository();
  List<Aya> ayahList;
  var _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  search(String text) async {
    ayaRepository.search(text).then((values) {
      setState(() {
        ayahList = values;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: <Widget>[
        IconButton(
            icon: Icon(
              Icons.cancel,
              color: Theme.of(context).bottomAppBarColor,
              size: 30,
            ),
            onPressed: () {
              Navigator.pop(context);
            }),
        Container(
          height: 50,
          color: Theme.of(context).hoverColor,
          padding: EdgeInsets.only(top: 8, right: 30, left: 30, bottom: 8),
          child: TextField(
            controller: _controller,
            autofocus: true,
            textInputAction: TextInputAction.search,
            toolbarOptions: ToolbarOptions(
                selectAll: true, copy: true, paste: true, cut: true),
            onSubmitted: (value) {
              if (value != null) {
                search(value);
              }
            },
            style:
                new TextStyle(color: Theme.of(context).primaryColor, height: 0),
            decoration: InputDecoration(
              suffixIcon: IconButton(
                onPressed: () => _controller.clear(),
                icon: Icon(Icons.clear,
                color: Theme.of(context).bottomAppBarColor,),
              ),
              enabledBorder: UnderlineInputBorder(
                borderSide:
                    BorderSide(color: Theme.of(context).bottomAppBarColor),
              ),
              focusedBorder: UnderlineInputBorder(
                borderSide:
                    BorderSide(color: Theme.of(context).bottomAppBarColor),
              ),
              hintText: AppLocalizations.of(context).search_word,
              hintStyle: TextStyle(
                  height: 1.5,
                  color: Theme.of(context).primaryColorLight.withOpacity(0.5),
                  fontFamily: 'cairo',
                  fontWeight: FontWeight.normal,
                  decorationColor: Theme.of(context).primaryColor,
                  fontSize: 14),
              contentPadding: EdgeInsets.only(top: 5, right: 16, left: 16),
            ),
          ),
        ),
        Expanded(
          child: Container(
              child: ayahList != null
                  ? ListView.builder(
                      itemCount: ayahList.length,
                      itemBuilder: (_, index) {
                        Aya aya = ayahList[index];
                        return Column(
                          children: <Widget>[
                            Container(
                              color: (index % 2 == 0
                                  ? Theme.of(context).hoverColor
                                  : Theme.of(context).hoverColor),
                              child: ListTile(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      new MaterialPageRoute(
                                          builder: ((context) => QuranShow(
                                              initialPageNum:
                                                  int.parse(aya.pageNum)))));
                                },
                                title: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: RichText(
                                      textAlign: TextAlign.justify,
                                      text: TextSpan(
                                          style: TextStyle(
                                              fontFamily: "Uthmanic",
                                              fontWeight: FontWeight.normal,
                                              fontSize: 22,
                                              color: Theme.of(context).primaryColorDark),
                                          text: aya.text,
                                          children: [
                                            WidgetSpan(
                                                child: ayaNum("${aya.ayaNum}", context, Theme.of(context).primaryColor),
                                            )
                                          ])),
                                ),
                                subtitle: Flex(
                                  direction: Axis.horizontal,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: <Widget>[
                                    Expanded(
                                      child: Container(
                                        color: Theme.of(context).primaryColor,
                                        child: Text(
                                          "${AppLocalizations.of(context).sorah}: ${aya.sorahName}",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              color:
                                                  Theme.of(context).hoverColor),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Container(
                                          color: Theme.of(context)
                                              .primaryColorLight,
                                          child: Text(
                                            " ${AppLocalizations.of(context).part}: ${aya.partNum}",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .hoverColor),
                                          )),
                                    ),
                                    Expanded(
                                      child: Container(
                                          color: Theme.of(context).primaryColor,
                                          //height: 60,
                                          child: Text(
                                            " ${AppLocalizations.of(context).page}: ${aya.pageNum}",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .hoverColor),
                                          )),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Divider()
                          ],
                        );
                      })
                  : Padding(
                      padding: EdgeInsets.all(30),
                      child: Text(
                        AppLocalizations.of(context).search_description,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 16,
                            color: Theme.of(context)
                                .bottomAppBarColor
                                .withOpacity(0.5),
                            fontFamily: "cairo"),
                      ),
                    )),
        ),
      ],
    );
  }
}

Widget ayaNum(String num, context, Color color) {
  return Padding(
    padding: const EdgeInsets.only(left: 5, right: 5),
    child: Transform.translate(
      offset: Offset(0, 0),
      child: Container(
        width: 26,
        height: 26,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/aya3.png"),
            fit: BoxFit.contain,
          ),
        ),
        child:
            Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 5),
            child: Text(
              num,
              style: TextStyle(
                  fontSize: 13,
                  fontWeight: FontWeight.bold,
                  fontFamily: "maddina",
                  color: color),
            ),
          )
        ]),
      ),
    ),
  );
}
