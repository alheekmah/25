import 'dart:async';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:preferences/preference_service.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    startTime();
  }

  startTime() async {
    var _duration = new Duration(seconds: 1);
    return new Timer(_duration, navigationPage);
  }

  void navigationPage() {
    String routeName = "/HomeScreen";
    if (PrefService.getBool("is_first_time")) {
      routeName = "/onboardingScreen";
      PrefService.setBool("is_first_time", false);
    }
    Navigator.of(context).pushReplacementNamed(routeName);
  }

  @override
  Widget build(BuildContext context) {
    Orientation orientation = MediaQuery.of(context).orientation;
    if (orientation == Orientation.portrait) {
      return Scaffold(
        body: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [
                Theme.of(context).hoverColor,
                Theme.of(context).hoverColor,
                Theme.of(context).hoverColor,
              ],
            ),
          ),
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.only(top: 32),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              SizedBox(
                height: 120,
              ),
              Image.asset(
                "assets/images/splash_icon.png",
                scale: 2.5,
              ),
              SizedBox(height: 64),
              Container(
                child: Image.asset(
                  "assets/images/alheemah.png",
                  scale: 9,
                  color: Theme.of(context).primaryColorLight,
                ),
              ),
              SizedBox(
                height: 32,
              ),
              Container(
                  height: 70,
                  width: MediaQuery.of(context).size.width,
                  color: Theme.of(context).accentColor,
                  child: Transform.rotate(
                    angle: pi, // 180 deg,
                    child: Image.asset(
                      'assets/images/zakh.png',
                      fit: BoxFit.cover,
                      color: Theme.of(context).hoverColor.withOpacity(0.4),
                    ),
                  ))
            ],
          ),
        ),
      );
    } else {
      return Scaffold(
        body: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [
                Theme.of(context).hoverColor,
                Theme.of(context).hoverColor,
                Theme.of(context).hoverColor,
              ],
            ),
          ),
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.only(top: 32),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              SizedBox(
                width: 32,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  SizedBox(
                    width: 72,
                  ),
                  Image.asset(
                    "assets/images/splash_icon.png",
                    scale: 2.5,
                  ),
                  SizedBox(width: 72),
                  Container(
                    child: Image.asset(
                      "assets/images/alheemah.png",
                      scale: 9,
                      color: Theme.of(context).primaryColorLight,
                    ),
                  ),
                  SizedBox(
                    width: 32,
                  ),
                ],
              ),
              Container(
                  height: 40,
                  width: MediaQuery.of(context).size.width,
                  color: Theme.of(context).accentColor,
                  child: Transform.rotate(
                    angle: pi, // 180 deg,
                    child: Image.asset(
                      'assets/images/zakh.png',
                      fit: BoxFit.cover,
                      color: Theme.of(context).hoverColor.withOpacity(0.4),
                    ),
                  ))
            ],
          ),
        ),
      );
    }
  }
}
