import 'dart:convert';
import 'package:alquranalkareem/module/quran/quran_search.dart';
import 'package:alquranalkareem/custom_clipper/drawClip.dart';
import 'package:alquranalkareem/module/quran/show.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class QuranJuz extends StatefulWidget {
  @override
  _QuranJuzState createState() => _QuranJuzState();
}

class _QuranJuzState extends State<QuranJuz>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      value: 0.0,
      duration: Duration(seconds: 25),
      upperBound: 1,
      lowerBound: -1,
      vsync: this,
    )..repeat();
  }

  @override
  void dispose() {
    _controller.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double padding = MediaQuery.of(context).size.height;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Theme.of(context).hoverColor,
        body: Stack(
          alignment: Alignment.topCenter,
          children: [
            AnimatedBuilder(
              animation: _controller,
              builder: (BuildContext context, Widget child) {
                return ClipPath(
                  clipper: DrawClip(_controller.value),
                  child: Container(
                    height: size.height * 0.2,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                          begin: Alignment.bottomLeft,
                          end: Alignment.topRight,
                          colors: [
                            Theme.of(context).primaryColor,
                            Theme.of(context).bottomAppBarColor
                          ]),
                    ),
                  ),
                );
              },
            ),
            Container(
              alignment: Alignment.topCenter,
              padding: EdgeInsets.only(top: 32),
              child: Image.asset(
                'assets/images/juz.png',
                scale: 13.0,
                color: Theme.of(context).hoverColor,
              ),
            ),
            Align(
              alignment: Alignment.topRight,
              child: IconButton(
                  icon: Icon(Icons.arrow_back),
                  color: Theme.of(context).hoverColor,
                  onPressed: () {
                    Navigator.pop(context);
                  }),
            ),
            Padding(
              padding: EdgeInsets.only(top: padding *.2),
              child: Divider(
                endIndent: 32,
                indent: 32,
                thickness: 2,
              ),
            ),
            Directionality(
              textDirection: TextDirection.rtl,
              child: Padding(
                  padding: EdgeInsets.only(top: padding *.21),
                  child: FutureBuilder(
                    builder: (context, snapshot) {
                      var showData = json.decode(snapshot.data.toString());
                      if (snapshot.connectionState == ConnectionState.done) {
                        return ListView.builder(
                          itemBuilder: (BuildContext context, int index) {
                            return GestureDetector(
                              child: Column(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 16),
                                    child: Container(
                                      width: MediaQuery.of(context).size.width,
                                      decoration: BoxDecoration(
                                          gradient: LinearGradient(
                                              begin: Alignment.bottomLeft,
                                              end: Alignment.topRight,
                                              colors: [
                                                Theme.of(context).primaryColor,
                                                Theme.of(context)
                                                    .bottomAppBarColor
                                              ]),
                                          borderRadius: BorderRadius.only(
                                              topRight: Radius.circular(8),
                                              topLeft: Radius.circular(8))),
                                      child: Column(
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 8),
                                            child: Row(
                                              children: [
                                                Container(
                                                  decoration: BoxDecoration(
                                                      color: Theme.of(context)
                                                          .hoverColor,
                                                      borderRadius:
                                                          BorderRadius.only(
                                                              topLeft: Radius
                                                                  .circular(2),
                                                              bottomLeft: Radius
                                                                  .circular(
                                                                      2))),
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            8.0),
                                                    child: Text(
                                                      'الجُزْءُ ${showData[index]['index2']}',
                                                      style: TextStyle(
                                                        color: Theme.of(context)
                                                            .primaryColorLight,
                                                        fontFamily: 'Tajawal',
                                                        fontSize: 15,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                Stack(
                                                  alignment: Alignment.center,
                                                  children: [
                                                    Image.asset(
                                                      'assets/images/sora_num.png',
                                                      scale: 10,
                                                    ),
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              top: 6),
                                                      child: Text(
                                                        '${showData[index]['index']}',
                                                        style: TextStyle(
                                                          color: Theme.of(
                                                                  context)
                                                              .primaryColorLight,
                                                          fontFamily: 'Tajawal',
                                                          fontSize: 15,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                            ),
                                          ),
                                          Padding(
                                            padding: EdgeInsets.all(8),
                                            child: Column(
                                              children: [
                                                Container(
                                                  width: 100,
                                                  decoration: BoxDecoration(
                                                      color: Theme.of(context)
                                                          .hoverColor,
                                                      borderRadius:
                                                          BorderRadius.only(
                                                              topRight: Radius
                                                                  .circular(8),
                                                              topLeft: Radius
                                                                  .circular(
                                                                      8))),
                                                  child: Text(
                                                    'من الآية ${showData[index]['start']['verse']}',
                                                    style: TextStyle(
                                                        fontFamily: "Uthmanic",
                                                        fontWeight:
                                                            FontWeight.normal,
                                                        fontSize: 18,
                                                        color: Theme.of(context)
                                                            .primaryColorLight),
                                                  ),
                                                  alignment: Alignment.center,
                                                ),
                                                Container(
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  decoration: BoxDecoration(
                                                      color: Theme.of(context)
                                                          .hoverColor,
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  4))),
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            8.0),
                                                    child: RichText(
                                                        textAlign:
                                                            TextAlign.justify,
                                                        text: TextSpan(
                                                            style: TextStyle(
                                                                fontFamily:
                                                                    "Uthmanic",
                                                                fontWeight:
                                                                    FontWeight
                                                                        .normal,
                                                                fontSize: 22,
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorLight),
                                                            text: showData[
                                                                        index]
                                                                    ['start']
                                                                ['ayatext'],
                                                            children: [
                                                              WidgetSpan(
                                                                  child: ayaNum(
                                                                      "${showData[index]['start']['verse']}",
                                                                      context,
                                                                      Theme.of(
                                                                              context)
                                                                          .primaryColorLight),
                                                                  style: TextStyle(
                                                                      color: Theme.of(
                                                                              context)
                                                                          .hoverColor)),
                                                            ])),
                                                  ),
                                                ),
                                                SizedBox(
                                                  height: 16,
                                                ),
                                                Container(
                                                  width: 100,
                                                  decoration: BoxDecoration(
                                                      color: Theme.of(context)
                                                          .hoverColor,
                                                      borderRadius:
                                                          BorderRadius.only(
                                                              topRight: Radius
                                                                  .circular(8),
                                                              topLeft: Radius
                                                                  .circular(
                                                                      8))),
                                                  child: Text(
                                                    'إلى الآية ${showData[index]['end']['verse']}',
                                                    style: TextStyle(
                                                        fontFamily: "Uthmanic",
                                                        fontWeight:
                                                            FontWeight.normal,
                                                        fontSize: 18,
                                                        color: Theme.of(context)
                                                            .primaryColorLight),
                                                  ),
                                                  alignment: Alignment.center,
                                                ),
                                                Container(
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  decoration: BoxDecoration(
                                                      color: Theme.of(context)
                                                          .hoverColor,
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  4))),
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            8.0),
                                                    child: RichText(
                                                        textAlign:
                                                            TextAlign.justify,
                                                        text: TextSpan(
                                                            style: TextStyle(
                                                                fontFamily:
                                                                    "Uthmanic",
                                                                fontWeight:
                                                                    FontWeight
                                                                        .normal,
                                                                fontSize: 22,
                                                                color: Theme.of(
                                                                        context)
                                                                    .primaryColorLight),
                                                            text:
                                                                showData[index]
                                                                        ['end']
                                                                    ['ayatext'],
                                                            children: [
                                                              WidgetSpan(
                                                                  child: ayaNum(
                                                                      "${showData[index]['end']['verse']}",
                                                                      context,
                                                                      Theme.of(
                                                                              context)
                                                                          .primaryColorLight)),
                                                            ])),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 16,
                                  )
                                ],
                              ),
                              onTap: () {
                                Navigator.push(
                                    context,
                                    new MaterialPageRoute(
                                        builder: ((context) => QuranShow(
                                              initialPageNum: int.parse(
                                                  showData[index]['start']
                                                      ['pageNum']),
                                            ))));
                              },
                            );
                          },
                          itemCount: 30,
                        );
                      } else {
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                    },
                    future: DefaultAssetBundle.of(context)
                        .loadString("assets/juz.json"),
                  )),
            ),
          ],
        ),
      ),
    );
  }
}
