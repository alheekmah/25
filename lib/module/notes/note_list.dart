import 'package:alquranalkareem/custom_clipper/linePathClass.dart';
import 'package:alquranalkareem/helper/app_localizations.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:provider/provider.dart';
import 'package:alquranalkareem/data/moor_database.dart';
import 'package:responsive_grid/responsive_grid.dart';


class NoteList extends StatefulWidget {
  @override
  _NoteListState createState() => _NoteListState();
}

class _NoteListState extends State<NoteList> {
  AppDatabase database;
  String title;
  String description;
  Note editNote;

  @override
  void initState() {
    super.initState();
    title = null;
    description = null;
    editNote = null;
  }

  @override
  Widget build(BuildContext context) {
    database = Provider.of<AppDatabase>(context);
    // TODO: implement build
    return Scaffold(
      body: Container(
        color: Theme.of(context).backgroundColor,
        child: Stack(
          children: [
            ClipPath(
              clipper: LinePathClass(),
              child: Container(
                padding: EdgeInsets.only(bottom: 50),
                color: Theme.of(context).primaryColorLight,
                height: 180,
                alignment: Alignment.center,
                child: Stack(
                  children: [
                    Align(
                      alignment: Alignment.bottomLeft,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        child: Text(
                          AppLocalizations.of(context).notes,
                          style: TextStyle(
                            color: Theme.of(context).hoverColor,
                            fontSize: 26,
                            fontFamily: 'Tajawal',
                          ),
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topRight,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 16),
                        child: IconButton(
                          icon: Icon(
                            Icons.arrow_back,
                            color: Theme.of(context).hoverColor,
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Stack(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 180, bottom: 60),
                  child: StreamBuilder(
                    stream: database.watchAllNotes(),
                    builder: (context, AsyncSnapshot<List<Note>> snapshot) {
                      final tasks = snapshot.data ?? List();

                      return ListView.builder(
                        itemCount: tasks.length,
                        itemBuilder: (_, index) {
                          final itemTask = tasks[index];
                          return _buildListItem(itemTask, database);
                        },
                      );
                    },
                  ),
                ),
                Align(
                  alignment: Alignment.bottomLeft,
                  child: GestureDetector(
                    child: Container(
                      width: 160,
                      height: 50,
                      decoration: BoxDecoration(
                          color: Theme.of(context).primaryColorLight,
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(8))),
                      child: Center(
                        child: Text(
                          AppLocalizations.of(context).add_new_note,
                          style: TextStyle(
                              color: Theme.of(context).hoverColor,
                              fontFamily: 'Tajawal',
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                    ),
                    onTap: () {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return _newNoteDialog();
                          });
                    },
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildListItem(Note itemNote, AppDatabase database) {
    return Slidable(
      actionPane: SlidableDrawerActionPane(),
      actionExtentRatio: 0.25,
      secondaryActions: [
        IconSlideAction(
          caption: AppLocalizations.of(context).delete,
          color: Colors.red,
          icon: Icons.delete,
          onTap: () => database.deleteNote(itemNote),
        ),
      ],
      child: GestureDetector(
        child: ResponsiveGridRow(
          children: [
            ResponsiveGridCol(
                xs: 12,
                child: Card(
                  color: Theme.of(context).bottomAppBarColor.withOpacity(.6),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: [
                        Text(itemNote.title,
                            style: TextStyle(
                                color: Theme.of(context).hoverColor,
                                fontSize: 18,
                                fontFamily: 'cairo',
                                fontWeight: FontWeight.bold)),
                        Divider(
                          endIndent: 32,
                          indent: 32,
                        ),
                        Text(itemNote.description,
                            style: TextStyle(
                                color: Theme.of(context)
                                    .hoverColor
                                    .withOpacity(.7),
                                fontSize: 14,
                                fontFamily: 'cairo',
                                fontWeight: FontWeight.w400)),
                      ],
                    ),
                  ),
                ))
          ],
        ),
        onTap: () {
          setState(() {
            editNote = itemNote;
            title = itemNote.title;
            description = itemNote.description;
          });
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return _newNoteDialog();
              });
        },
      ),
    );
  }

  Widget _newNoteDialog() {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(2)),
      ),
      title: Text(
        editNote == null
            ? AppLocalizations.of(context).add_new_note
            : AppLocalizations.of(context).edit,
        style: TextStyle(
            color: Theme.of(context).bottomAppBarColor,
            fontWeight: FontWeight.w600),
      ),
      actions: <Widget>[
        Align(
          alignment: Alignment.bottomLeft,
          child: GestureDetector(
            child: Container(
              width: 160,
              height: 50,
              decoration: BoxDecoration(
                  color: Theme.of(context).primaryColorLight,
                  borderRadius:
                      BorderRadius.only(topRight: Radius.circular(8))),
              child: Center(
                child: Text(
                  AppLocalizations.of(context).save,
                  style: TextStyle(
                      color: Theme.of(context).hoverColor,
                      fontFamily: 'Tajawal',
                      fontWeight: FontWeight.w500),
                ),
              ),
            ),
            onTap: () {
              if (editNote == null) {
                Note note =
                    new Note(id: null, title: title, description: description);
                database.insertNote(note);
              } else {
                Note note = new Note(
                    id: editNote.id, title: title, description: description);
                database.updateNote(note);
                setState(() {
                  editNote = null;
                });
              }

              Navigator.pop(context);
            },
          ),
        ),
      ],
      content: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
                height: 40,
                child: TextFormField(
                  initialValue: editNote != null ? editNote.title : null,
                  onChanged: (value) {
                    setState(() {
                      title = value.trim();
                    });
                  },
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(4),
                          borderSide: BorderSide(
                              color: Theme.of(context).bottomAppBarColor)),
                      hintText: AppLocalizations.of(context).note_title,
                      suffixIcon: const Icon(
                        FontAwesome.list_alt,
                        color: Color(0xff91a57d),
                      ),
                      hintStyle: TextStyle(
                          height: 0.8, fontSize: 14, fontFamily: 'cairo')),
                )),
            SizedBox(
              height: 10,
            ),
            Container(
                child: TextFormField(
              initialValue: editNote != null ? editNote.description : null,
              textAlign: TextAlign.right,
              maxLines: null,
              keyboardType: TextInputType.multiline,
              onChanged: (value) {
                setState(() {
                  description = value.trim();
                });
              },
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(4),
                      borderSide: BorderSide(
                          color: Theme.of(context).bottomAppBarColor)),
                  hintText: AppLocalizations.of(context).note_details,
                  hintStyle: TextStyle(
                      height: 0.12, fontFamily: 'cairo', fontSize: 14)),
            )),
          ],
        ),
      ),
      titleTextStyle: TextStyle(
          fontSize: 18, fontFamily: 'cairo', fontWeight: FontWeight.w500),
    );
  }
}

