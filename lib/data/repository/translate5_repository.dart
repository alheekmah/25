import 'package:alquranalkareem/data/model/ayat.dart';
import 'package:alquranalkareem/data/model/translate.dart';
import 'package:alquranalkareem/data/tafseer_data_client.dart';
import 'package:sqflite/sqflite.dart';

class TranslateRepository5 {
  TafseerDataBaseClient _client;
  TranslateRepository5(){
    _client = TafseerDataBaseClient.instance;
  }

  Future<List<Ayat>> getPageTranslate(int pageNum) async {
    Database database = await _client.database;
    List<Map> results = await database
        .rawQuery(" SELECT * FROM ${Ayat.tableName} "
        "LEFT JOIN ${Translate.tableName5} "
        "ON (${Translate.tableName5}.aya = ${Ayat.tableName}.Verse) AND (${Translate.tableName5}.sura = ${Ayat.tableName}.SuraNum) "
        "WHERE PageNum = $pageNum");
    List<Ayat> ayaList =  List();
    results.forEach((result) {
      ayaList.add(Ayat.fromMap(result));
    });
    return ayaList;
  }
}