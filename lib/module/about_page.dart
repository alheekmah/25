import 'package:alquranalkareem/custom_clipper/linePathClass.dart';
import 'package:alquranalkareem/helper/app_localizations.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:alquranalkareem/module/quran/widgets/share.dart';

class AboutPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final share = Provider.of<Share>(context);
    // TODO: implement build
    return Scaffold(
      backgroundColor: Theme.of(context).hoverColor,
      body: SingleChildScrollView(
        child: Scrollbar(
          child: Stack(
            children: [
              ClipPath(
                clipper: LinePathClass(),
                child: Container(
                  padding: EdgeInsets.only(bottom: 50),
                  color: Theme.of(context).primaryColorLight,
                  height: 180,
                  alignment: Alignment.center,
                  child: Stack(
                    children: [
                      Center(
                          child: Image.asset(
                        "assets/images/zakh2.png",
                        width: MediaQuery.of(context).size.width,
                        color: Theme.of(context).hoverColor,
                      )),
                      Align(
                        alignment: Alignment.bottomLeft,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          child: Text(
                            AppLocalizations.of(context).about_us,
                            style: TextStyle(
                              color: Theme.of(context).hoverColor,
                              fontSize: 26,
                              fontFamily: 'Tajawal',
                            ),
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.topRight,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical: 28),
                          child: IconButton(
                            icon: Icon(
                              Icons.arrow_back,
                              color: Theme.of(context).hoverColor,
                            ),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  SizedBox(
                    height: 200,
                  ),
                  Center(
                    child: Text(
                      AppLocalizations.of(context).stop_title,
                      textAlign: TextAlign.justify,
                      style: TextStyle(
                          fontSize: 20,
                          color: Theme.of(context).primaryColorLight,
                          fontFamily: 'Tajawal',
                          fontWeight: FontWeight.w700),
                    ),
                  ),
                  Divider(
                    height: 5,
                    endIndent: 32,
                    indent: 32,
                  ),
                  Divider(
                    height: 0,
                    endIndent: 32,
                    indent: 32,
                  ),
                  SizedBox(
                    height: 32,
                  ),
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: Text(
                        AppLocalizations.of(context).about_app,
                        textAlign: TextAlign.justify,
                        style: TextStyle(
                            fontFamily: 'Tajawal',
                            fontSize: 18,
                            color: Theme.of(context).primaryColorLight),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 32,
                  ),
                  Container(
                    height: 25,
                    width: MediaQuery.of(context).size.width,
                    color: Theme.of(context).bottomAppBarColor,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: Text(
                        AppLocalizations.of(context).about_app2,
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            fontFamily: 'Tajawal',
                            fontSize: 20,
                            color: Theme.of(context).hoverColor,
                            fontWeight: FontWeight.w700),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: Text(
                      AppLocalizations.of(context).about_app3,
                      textAlign: TextAlign.justify,
                      style: TextStyle(
                        fontFamily: 'Tajawal',
                        fontSize: 18,
                        color: Theme.of(context).primaryColorLight,
                      ),
                    ),
                  ),
                  Divider(),
                  SizedBox(
                    height: 20,
                  ),
                  Center(
                    child: SelectableText(
                      AppLocalizations.of(context).email,
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 20, color: Colors.blueGrey),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Center(
                      child: Image.asset(
                    "assets/images/alheemah.png",
                    width: 220,
                    color: Theme.of(context).primaryColorLight,
                  )),
                  Align(
                    alignment: Alignment.bottomLeft,
                    child: Container(
                      width: 50,
                      height: 50,
                      decoration: BoxDecoration(
                          color: Theme.of(context).primaryColorLight,
                          borderRadius:
                              BorderRadius.only(topRight: Radius.circular(8))),
                      child: Center(
                          child: IconButton(
                        icon: Icon(
                          Icons.share,
                          color: Theme.of(context).hoverColor,
                        ),
                        onPressed: () {
                          // share(context);
                          share.share();
                        },
                      )),
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }


}
