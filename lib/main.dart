import 'package:alquranalkareem/module/quran/widgets/share.dart';
import 'package:alquranalkareem/themes/custom_theme.dart';
import 'package:alquranalkareem/themes/themes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:preferences/preferences.dart';
import 'package:alquranalkareem/data/data_client.dart';
import 'package:alquranalkareem/data/tafseer_data_client.dart';
import 'package:provider/provider.dart';

import 'athkar/azkar_categories_list.dart';
import 'athkar/favourites.dart';
import 'athkar/push_notification_test_methods.dart';
import 'athkar/zekr_details.dart';
import 'helper/settings_helpers.dart';
import 'my_app.dart';

main() async {
  await init();

  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(create: (context){
        return Share();
      })
    ],
    child: RestartWidget(
          child: CustomTheme(
              initialThemeKey: MyThemeKeys.LIGHT,
               child: MyApp()),
    ),
  ));
}

init() async {
  WidgetsFlutterBinding.ensureInitialized();
  await PrefService.init(prefix: 'pref_');
  await SettingsHelpers.instance.init();
  Map<String, Object> defaultValues = new Map();
  defaultValues["audio_player_sound"] = "Minshawy_Murattal_128kbps";
  defaultValues["start_page"] = 1;
  defaultValues["locale"] = "ar";
  defaultValues["is_first_time"] = true;
  PrefService.setDefaultValues(defaultValues);
  DataBaseClient dataBaseClient = DataBaseClient.instance;
  dataBaseClient.initDatabase();
  TafseerDataBaseClient tafseerDataBaseClient = TafseerDataBaseClient.instance;
  tafseerDataBaseClient.initDatabase();
}

class RestartWidget extends StatefulWidget {
  final Widget child;

  RestartWidget({this.child});


  @override
  _RestartWidgetState createState() => new _RestartWidgetState();
}

class _RestartWidgetState extends State<RestartWidget> {
  final List<Map<String, String>> mappedData = mappedJson;
  final List<String> onlyAzkarList = [];
  var finalCategoryList = [];
  var _enableNotifivations = true;
  // var notification;

  FlutterLocalNotificationsPlugin notification =
  FlutterLocalNotificationsPlugin();
  // FlutterLocalNotificationsPlugin notification2 =
  // FlutterLocalNotificationsPlugin();


  @override
  void initState() {
    super.initState();
    var androidSettings = AndroidInitializationSettings('@mipmap/ic_launcher');
    var IOSSettings = IOSInitializationSettings(
        requestSoundPermission: true,
        requestBadgePermission: true,
        requestAlertPermission: true,
        onDidReceiveLocalNotification: (id, title, body, payload) =>
            onSelectNotification(payload));
    var initSettings = InitializationSettings(androidSettings, IOSSettings);
    notification.initialize(
        initSettings, onSelectNotification: onSelectNotification);
  }

  Future onSelectNotification(String payload) async {
    notification.cancelAll();
    return await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ZekrList.fromFont(
                  category: payload,
                  font: 'Tajawal',
                )));
  }

  @override
  Widget build(BuildContext context) {
    EnableNotifications.readFromFile().then((contents) {
      var filecontenst = contents;
      if (filecontenst == '') {
        showNotification3(
            notification: notification,
            channelId: 1,
            title: "أذكار الصباح",
            time: Time(6, 0, 0));
        showNotification3(
            notification: notification,
            channelId: 2,
            title: "أذكار المساء",
            time: Time(14, 28, 0));
        showNotification3(
            notification: notification,
            channelId: 3,
            title: "أذكار الاستيقاظ من النوم",
            time: Time(7, 0, 0));
        showNotification3(
            notification: notification,
            channelId: 4,
            title: "أذكار النوم",
            time: Time(22, 0, 0));
        debugPrint('notifications enabled');
        EnableNotifications.enable();
      } else {
        debugPrint("any habal");
      }
    });

    for (int c = 0; c < mappedData.length; c++) {
      onlyAzkarList.add(mappedData[c]["category"]);
    }
    finalCategoryList = onlyAzkarList.toSet().toList();
    debugPrint("nuber of ${finalCategoryList.length}");

    return KeyedSubtree(
      child: widget.child,
    );
  }
}
