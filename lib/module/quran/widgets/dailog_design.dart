import 'package:flutter/material.dart';

class DailogDesign extends StatefulWidget {
  String titleName;
  Widget wid;

  DailogDesign(this.titleName, this.wid);

  @override
  _DailogDesignState createState() => _DailogDesignState(titleName, wid);
}

class _DailogDesignState extends State<DailogDesign> {
  String titleName;
  Widget wid;

  _DailogDesignState(this.titleName, this.wid);

  @override
  Widget build(BuildContext context) {
    return Container(
            height: MediaQuery.of(context).size.height /
                1 /
                2 *
                0.9,
            child: Stack(
              children: <Widget>[
                Container(
                  height:
                  MediaQuery.of(context).size.height /
                      1 /
                      2 *
                      1.0,
                  decoration: BoxDecoration(
                    borderRadius:
                    BorderRadius.circular(8),
                    color: Theme.of(context)
                        .hoverColor
                        .withOpacity(0.8),
                  ),
                  width: 300,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 64),
                  child: Container(
                    height: MediaQuery.of(context)
                        .size
                        .height /
                        1 /
                        2 *
                        0.9,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30)),
                      color:
                      Theme.of(context).primaryColor,
                    ),
                    width: 300,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 64),
                  child: Container(
                    margin: EdgeInsets.only(
                        left: 90, bottom: 20),
                    width: 200,
                    height: MediaQuery.of(context)
                        .size
                        .height /
                        1 /
                        2 *
                        0.8,
                    decoration: BoxDecoration(
                        color: Theme.of(context)
                            .bottomAppBarColor
                            .withOpacity(0.07),
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(160),
                            bottomLeft:
                            Radius.circular(290),
                            bottomRight:
                            Radius.circular(160),
                            topRight:
                            Radius.circular(10))),
                  ),
                ),
                Container(
                  child: Padding(
                    padding:
                    const EdgeInsets.only(top: 16),
                    child: Text(
                      titleName,
                      style: TextStyle(
                          fontSize: 20.0,
                          fontFamily: 'Tajawal',
                          fontWeight: FontWeight.w400,
                          backgroundColor:
                          Theme.of(context)
                              .primaryColorLight
                              .withOpacity(0.9),
                          color: Theme.of(context)
                              .hoverColor),
                    ),
                  ),
                  alignment: Alignment.topCenter,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 70),
                  child: Container(
                    width: 300,
                    height: MediaQuery.of(context)
                        .size
                        .height /
                        1 /
                        2 *
                        0.8,
                    child: wid
                  ),
                ),
              ],
            ),
    );
  }
}
