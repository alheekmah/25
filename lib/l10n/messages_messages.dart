// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a messages locale. All the
// messages from the main program should be duplicated here with the same
// function name.

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

// ignore: unnecessary_new
final messages = new MessageLookup();

// ignore: unused_element
final _keepAnalysisHappy = Intl.defaultLocale;

// ignore: non_constant_identifier_names
typedef MessageIfAbsent(String message_str, List args);

class MessageLookup extends MessageLookupByLibrary {
  get localeName => 'messages';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "appName" : MessageLookupByLibrary.simpleMessage("Alquran Alkareem"),
    "search_hint" : MessageLookupByLibrary.simpleMessage("Search the verses of the Quran"),
    "menu" : MessageLookupByLibrary.simpleMessage("Menu"),
    "notes" : MessageLookupByLibrary.simpleMessage("Notes"),
    "note_title" : MessageLookupByLibrary.simpleMessage("Note Title"),
    "add_new_note" : MessageLookupByLibrary.simpleMessage("Add a new note"),
    "note_details" : MessageLookupByLibrary.simpleMessage("Note Details"),
    "bookmarks" : MessageLookupByLibrary.simpleMessage("Bookmarks Saved"),
    "bookmark_title" : MessageLookupByLibrary.simpleMessage("Bookmark Name"),
    "add_new_bookmark" : MessageLookupByLibrary.simpleMessage("Add a new Bookmark"),
    "save" : MessageLookupByLibrary.simpleMessage("Save"),
    "edit" : MessageLookupByLibrary.simpleMessage("Edit"),
    "azkar" : MessageLookupByLibrary.simpleMessage("Fortress of the Muslim"),
    "qibla" : MessageLookupByLibrary.simpleMessage("qibla direction"),
    "salat" : MessageLookupByLibrary.simpleMessage("Prayer Times"),
    "aya_count" : MessageLookupByLibrary.simpleMessage("Number of verses"),
    "quran_sorah" : MessageLookupByLibrary.simpleMessage("All Sorah"),
    "about_us" : MessageLookupByLibrary.simpleMessage("About App"),
    "stop_title" : MessageLookupByLibrary.simpleMessage("The noble Qur’an - Al-Heekmah Library"),
    "about_app" : MessageLookupByLibrary.simpleMessage("The application of the Qur’an Al Kareem is characterized by its approval of the edition of the King Fahd Complex for Printing the Noble Qur’an in Madinah because of its reliability and mastery.\nThe Quran Al Kareem application is sponsored by the \"Al-Heekmah Library\".\nIt is a well-designed application that allows you to read interactively with the text of the electronic Quran, listen to recitations, study the  Quran Al Kareem and memorize it with ease."),
    "about_app3" : MessageLookupByLibrary.simpleMessage("◉ Reading in portrait and landscape mode.\n◉ The application facilitates the text search feature in the verses of the Qur'an through the instant search and display of results with pages in addition to the ability to go to the page.\n◉ Save reading sites so that the reader can save the page and return to it whenever he wants.\n◉ Add notes.\n◉ Listen to the sura of the Qur’an with the reading of the two sheikhs, \"Mahmoud Khalil Al-Hosary, Muhammad Siddiq Al-Minshawi.\".\n◉ The application provides tafseer for each verse.\n◉ Change the interpretation.\n◉ Change the font size of the tafsir.\n◉ Sura list.\n◉ Easily navigate between the sura.\n◉ The application allows reading the rulings of recitations.\n◉ The application features the addition of a complete Hisn Al Muslim and divided according to the adhkar so that the reader can easily navigate between sections.\n◉ Add any section to favorites.\n◉ The application provides the reader with night reading, which stains the background in black and white lines, to give the reader complete comfort when reading in low light environments."),
    "about_app2" : MessageLookupByLibrary.simpleMessage("Among the most important features of the application :"),
    "email" : MessageLookupByLibrary.simpleMessage("Library communication account :\nalheekmahlib@gmail.com"),
    "select_player" : MessageLookupByLibrary.simpleMessage("Reader selected"),
    "delete" : MessageLookupByLibrary.simpleMessage("Delete"),
    "page" : MessageLookupByLibrary.simpleMessage("Page"),
    "search_word" : MessageLookupByLibrary.simpleMessage("Ayah To search"),
    "search_description" : MessageLookupByLibrary.simpleMessage("You can search for all verses of the Noble Qur’an, just type a word from the verse."),
    "fontSize" : MessageLookupByLibrary.simpleMessage("Change Font Size"),
    "waqfName" : MessageLookupByLibrary.simpleMessage("Stop Signs"),
    "onboardTitle1" : MessageLookupByLibrary.simpleMessage("Easy interface"),
    "onboardDesc1" : MessageLookupByLibrary.simpleMessage("- Ease of searching for a verse. \n- Change the language. \n- Listen to the page. \n- Change the reader."),
    "onboardTitle2" : MessageLookupByLibrary.simpleMessage("Show The Tafseer"),
    "onboardDesc2" : MessageLookupByLibrary.simpleMessage("The interpretation of each verse can be read by pulling the list up."),
    "onboardTitle3" : MessageLookupByLibrary.simpleMessage("Click Options"),
    "onboardDesc3" : MessageLookupByLibrary.simpleMessage("1- When you double click the page is enlarged.\n2- Upon long click you will be presented with the option to save the page.\n3- When you press once, the menus appear."),
    "light" : MessageLookupByLibrary.simpleMessage("Light"),
    "dark" : MessageLookupByLibrary.simpleMessage("Dark"),
    "azkarfav" : MessageLookupByLibrary.simpleMessage("Athkar Favorite"),
    "themeTitle" : MessageLookupByLibrary.simpleMessage("Night reading"),
    "sorah" : MessageLookupByLibrary.simpleMessage("Sorah"),
    "part" : MessageLookupByLibrary.simpleMessage("Part"),
    "langChange" : MessageLookupByLibrary.simpleMessage("Change the language"),
    "tafChange" : MessageLookupByLibrary.simpleMessage('Choose an tafsir'),
    "tafIbnkatheerN" : MessageLookupByLibrary.simpleMessage('Tafsir Ibn Kathir'),
    "tafBaghawyN" : MessageLookupByLibrary.simpleMessage('Tafsir al-Baghawi'),
    "tafQurtubiN" : MessageLookupByLibrary.simpleMessage('Tafsir al-Qurtubi'),
    "tafSaadiN" : MessageLookupByLibrary.simpleMessage('Tafsir As-Sadi'),
    "tafTabariN" : MessageLookupByLibrary.simpleMessage('Tafsir al-Tabari'),
    "tafIbnkatheerD" : MessageLookupByLibrary.simpleMessage('Tafsir al-Quran al-Azim'),
    "tafBaghawyD" : MessageLookupByLibrary.simpleMessage('Maalim al-Tanzil'),
    "tafQurtubiD" : MessageLookupByLibrary.simpleMessage('Al-Jami li Ahkam al-Quran'),
    "tafSaadiD" : MessageLookupByLibrary.simpleMessage('Taiseer Kalam Almannan'),
    "tafTabariD" : MessageLookupByLibrary.simpleMessage('Jami al-Bayan an Tawil [ay] al Quran'),
    "appLang" : MessageLookupByLibrary.simpleMessage('App Language'),
    "setting" : MessageLookupByLibrary.simpleMessage('Setting'),
    "reader1" : MessageLookupByLibrary.simpleMessage('Mahmoud Khalil Al-Hussary'),
    "reader2" : MessageLookupByLibrary.simpleMessage('Mohamed Siddiq El-Minshawi'),
    "alheekmahlib" : MessageLookupByLibrary.simpleMessage('Alheekmah Library'),
    "backTo" : MessageLookupByLibrary.simpleMessage('Back To'),
    "next" : MessageLookupByLibrary.simpleMessage('Next'),
    "start" : MessageLookupByLibrary.simpleMessage('Start'),
  };
}
