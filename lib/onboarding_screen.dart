import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'helper/app_localizations.dart';

class OnboardingScreen extends StatefulWidget {
  @override
  _OnboardingScreenState createState() => _OnboardingScreenState();
}

class _OnboardingScreenState extends State<OnboardingScreen> {
  final int _numPages = 3;
  final PageController _pageController = PageController(initialPage: 0);
  int _currentPage = 0;

  List<Widget> _buildPageIndicator() {
    List<Widget> list = [];
    for (int i = 0; i < _numPages; i++) {
      list.add(i == _currentPage ? _indicator(true) : _indicator(false));
    }
    return list;
  }

  Widget _indicator(bool isActive) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 150),
      margin: EdgeInsets.symmetric(horizontal: 8.0),
      height: 8.0,
      width: isActive ? 24.0 : 16.0,
      decoration: BoxDecoration(
        color: isActive ? Theme.of(context).bottomAppBarColor : Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(12)),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              stops: [0.1, 0.4, 0.7, 0.9],
              colors: [
                Color(0xfff3efdc),
                Color(0xfff3efdd),
                Color(0xfff3efde),
                Color(0xfff3efdf),
              ],
            ),
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 40.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Container(
                    alignment: Alignment.centerRight,
                    child: FlatButton(
                      color: Theme.of(context).bottomAppBarColor,
                      shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(8.0),
                      ),
                      onPressed: () =>
                          Navigator.of(context).pushNamed('/SettingPage'),
                      child: Text(
                        'تخطي',
                        style: TextStyle(
                          color: Theme.of(context).hoverColor,
                          fontSize: 20.0,
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).size.height * 3/4,
                  child: PageView(
                    physics: ClampingScrollPhysics(),
                    controller: _pageController,
                    onPageChanged: (int page) {
                      setState(() {
                        _currentPage = page;
                      });
                    },
                    children: <Widget>[
                      ListView(
                        children: <Widget>[
                        Padding(
                          padding: EdgeInsets.all(16.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Center(
                                child: Image(
                                  image: AssetImage(
                                    'assets/images/face_show.png',
                                  ),
                                ),
                              ),
                              SizedBox(height: 30.0),
                              Container(
                                height:
                                    MediaQuery.of(context).size.height / 1 / 4,
                                child: Column(
                                  children: <Widget>[
                                    Text(
                                      AppLocalizations.of(context).onboardTitle1,
                                      style: TextStyle(
                                          fontFamily: 'cairo',
                                          fontWeight: FontWeight.w600,
                                          fontSize: 16,
                                          color:
                                              Theme.of(context).primaryColorDark),
                                    ),
                                    SizedBox(height: 15.0),
                                    Text(
                                      AppLocalizations.of(context).onboardDesc1,
                                      style: TextStyle(
                                          fontFamily: 'cairo',
                                          fontSize: 16,
                                          color: Theme.of(context)
                                              .primaryColorLight),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ]),
                      ListView(
                        children: <Widget>[
                        Padding(
                          padding: EdgeInsets.all(16.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Center(
                                child: Image(
                                  image: AssetImage(
                                    'assets/images/tafseer_show.png',
                                  ),
                                ),
                              ),
                              SizedBox(height: 30.0),
                              Text(
                                AppLocalizations.of(context).onboardTitle2,
                                style: TextStyle(
                                    fontFamily: 'cairo',
                                    fontWeight: FontWeight.w600,
                                    fontSize: 16,
                                    color: Theme.of(context).primaryColorDark),
                              ),
                              SizedBox(height: 15.0),
                              Text(
                                AppLocalizations.of(context).onboardDesc2,
                                style: TextStyle(
                                    fontFamily: 'cairo',
                                    fontSize: 16,
                                    color: Theme.of(context).primaryColorLight),
                              ),
                            ],
                          ),
                        ),
                      ]),
                      ListView(
                        children: <Widget>[
                        Padding(
                          padding: EdgeInsets.all(40.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Center(
                                child: Image(
                                  image: AssetImage(
                                    'assets/images/tap_show.png',
                                  ),
                                ),
                              ),
                              SizedBox(height: 30.0),
                              Text(
                                AppLocalizations.of(context).onboardTitle3,
                                style: TextStyle(
                                    fontFamily: 'cairo',
                                    fontWeight: FontWeight.w600,
                                    fontSize: 16,
                                    color: Theme.of(context).primaryColorLight),
                              ),
                              SizedBox(height: 15.0),
                              Text(
                                AppLocalizations.of(context).onboardDesc3,
                                style: TextStyle(
                                    fontFamily: 'cairo',
                                    fontSize: 16,
                                    color: Theme.of(context).primaryColorLight),
                              ),
                            ],
                          ),
                        ),
                      ]),
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: _buildPageIndicator(),
                ),
                _currentPage != _numPages - 1
                    ? Expanded(
                        child: Padding(padding: EdgeInsets.symmetric(horizontal: 16),
                          child: Align(
                            alignment: FractionalOffset.bottomLeft,
                            child: FlatButton(
                              color: Theme.of(context).bottomAppBarColor,
                              shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(8.0),
                                  ),
                              onPressed: () {
                                _pageController.nextPage(
                                  duration: Duration(milliseconds: 500),
                                  curve: Curves.ease,
                                );
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Text(
                                    AppLocalizations.of(context).next,
                                    style: TextStyle(
                                        color:
                                            Theme.of(context).hoverColor,
                                        fontSize: 18.0,
                                        fontFamily: 'cairo',
                                        fontWeight: FontWeight.w600),
                                  ),
                                  SizedBox(width: 10.0),
                                  Icon(
                                    Icons.arrow_forward,
                                    color: Theme.of(context).hoverColor,
                                    size: 28.0,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      )
                    : Text(''),
              ],
            ),
          ),
        ),
      ),
      bottomSheet: _currentPage == _numPages - 1
          ? GestureDetector(
        onTap: () =>
            Navigator.of(context).pushNamed('/SettingPage'),
            child: Container(
                width: double.infinity,
            constraints: BoxConstraints(minHeight: 6.5, maxHeight: 50),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.vertical(
                top: Radius.circular(15),
              ),
                color: Theme.of(context).bottomAppBarColor,
            ),
                child: Stack(
                  children: <Widget>[
                    Container(
                        height: MediaQuery.of(context).size.height,
                        width: MediaQuery.of(context).size.width,
                        child: Image.asset(
                          'assets/images/zakh2.png',
                          fit: BoxFit.cover,
                          color: Theme.of(context).hoverColor.withOpacity(0.2),
                        )),
                    Center(
                      child: Padding(
                        padding: EdgeInsets.only(bottom: 10.0),
                        child: Text(
                          AppLocalizations.of(context).start,
                          style: TextStyle(
                              color: Theme.of(context).hoverColor,
                              fontSize: 22.0,
                              fontFamily: 'cairo',
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
          )
          : Text(''),
    );
  }
}
