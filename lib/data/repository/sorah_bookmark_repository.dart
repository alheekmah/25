
import 'package:alquranalkareem/data/data_client.dart';
import 'package:alquranalkareem/data/model/sorah_bookmark.dart';
import 'package:sqflite/sqflite.dart';

class SorahBookmarkRepository {
  DataBaseClient _client;
  SorahBookmarkRepository(){
    _client = DataBaseClient.instance;
  }

//  Future<List<SoraBookmark>> all() async {
//    Database database = await _client.database;
//    List<Map> results = await database.query(SorahBookmark.tableName,columns: SorahBookmark.columns);
//    List<SorahBookmark> sorahBookmarkList =  List();
//    results.forEach((result) {
//      sorahBookmarkList.add(SorahBookmark.fromMap(result));
//    });
//    return sorahBookmarkList;
//  }

  Future<List<SoraBookmark>> all() async {
    Database database = await _client.database;
    List<Map> results = await database.query(SoraBookmark.tableName,columns: SoraBookmark.columns);
    List<SoraBookmark> soraBookmarkList =  List();
    results.forEach((result) {
      soraBookmarkList.add(SoraBookmark.fromMap(result));
    });
    return soraBookmarkList;
  }
}