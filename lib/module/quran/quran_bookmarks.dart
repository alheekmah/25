import 'package:alquranalkareem/helper/app_localizations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:provider/provider.dart';
import 'package:alquranalkareem/data/moor_database.dart';
import 'package:alquranalkareem/module/quran/show.dart';

class QuranBookmarks extends StatefulWidget {
  @override
  _QuranBookmarksState createState() => _QuranBookmarksState();
}

class _QuranBookmarksState extends State<QuranBookmarks> {
  AppDatabase database;
  String _title;
  Bookmark _editBookmark;
  @override
  void initState() {
    super.initState();
    _title = null;
    _editBookmark = null;
  }

//  saveBookmark(){
//    if(_editBookmark == null) {
//      newBookmark();
//    } else {
//      updateBookmark();
//    }
//  }

//  newBookmark() async {
//    var now = new DateTime.now();
//    String lastRead = "${now.year}-${now.month}-${now.day}";
//    Bookmark bookmark = new Bookmark(id: null, title: _title,pageNum: 1,lastRead: lastRead);
//    await database.insertBookmark(bookmark);
//  }

  updateBookmark() async {
    Bookmark bookmark = new Bookmark(
        id: _editBookmark.id,
        title: _title,
        pageNum: _editBookmark.pageNum,
        lastRead: _editBookmark.lastRead);
    await database.updateBookmark(bookmark);
    setState(() {
      _editBookmark = null;
    });
  }

  @override
  Widget build(BuildContext context) {
    database = Provider.of<AppDatabase>(context);
    // TODO: implement build
    return _buildBookmarkList(context);
  }

  StreamBuilder<List<Bookmark>> _buildBookmarkList(BuildContext context) {
    return StreamBuilder(
      stream: database.watchAllBookmarks(),
      builder: (context, AsyncSnapshot<List<Bookmark>> snapshot) {
        final bookmarks = snapshot.data ?? List();

        return ListView.builder(
          itemCount: bookmarks.length,
          itemBuilder: (_, index) {
            final bookmark = bookmarks[index];
//            Sorah sorah = sorahList[index];
            return Column(
              children: <Widget>[
                GestureDetector(
                  child: Slidable(
                    actionPane: SlidableDrawerActionPane(),
                    actionExtentRatio: 0.25,
                    secondaryActions: [
                      IconSlideAction(
                        caption: AppLocalizations.of(context).delete,
                        color: Colors.red,
                        icon: Icons.delete,
                        onTap: () => database.deleteBookmark(bookmark),
                      ),
                    ],
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            CircleAvatar(
                              radius: 25,
                              backgroundColor: Colors.transparent,
                              child: Stack(
                                children: <Widget>[
                                  Center(
                                    child: Image.asset(
                                      'assets/images/sora_num2.png',
                                      width: 40,
                                      height: 40,
                                      color: Theme.of(context).brightness ==
                                              Brightness.light
                                          ? null
                                          : Theme.of(context).primaryColorLight,
                                    ),
                                  ),
                                  Center(
                                    child: Text(
                                      "${bookmark.pageNum}",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Theme.of(context).hoverColor,
                                          fontFamily: 'naskh',
                                          fontWeight: FontWeight.w700,
                                          fontSize: 18),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text(
                              '${bookmark.title}',
                              style: TextStyle(
                                  color: Theme.of(context).primaryColorDark,
                                  fontSize: 16,
                                  fontFamily: 'cairo',
                                  fontWeight: FontWeight.w500),
                            ),
                            Text(
                              "${bookmark.lastRead}",
                              style: TextStyle(
                                  color: Theme.of(context).primaryColorLight,
                                  fontSize: 14,
                                  fontFamily: 'cairo',
                                  fontWeight: FontWeight.w400),
                            ),
                            Icon(
                              Icons.bookmark,
                              color: Color(0x99f5410a),
                              size: 35,
                            ),
                          ],
                        ),
                      ),
                  onTap: () {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: ((context) => QuranShow(
                                  initialPageNum: bookmark.pageNum,
                                  bookmark: bookmark,
                                ))));
                  },
                ),
                Divider(
                  height: 5,
                  endIndent: 32,
                  indent: 32,
                )
              ],
            );
          },
        );
      },
    );
  }
}
