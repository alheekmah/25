import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

// ignore: unnecessary_new
final messages = new MessageLookup();

// ignore: unused_element
final _keepAnalysisHappy = Intl.defaultLocale;

// ignore: non_constant_identifier_names
typedef MessageIfAbsent(String message_str, List args);

class MessageLookup extends MessageLookupByLibrary {
  get localeName => 'ar';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "appName" : MessageLookupByLibrary.simpleMessage("القرآن الكريم"),
    "search_hint" : MessageLookupByLibrary.simpleMessage("البحث في آيات القرآن الكريم"),
    "menu" : MessageLookupByLibrary.simpleMessage("القائمة"),
    "notes" : MessageLookupByLibrary.simpleMessage("الملاحظات"),
    "note_title" : MessageLookupByLibrary.simpleMessage("الملاحظات"),
    "add_new_note" : MessageLookupByLibrary.simpleMessage("اضافة ملاحظة جديدة"),
    "note_details" : MessageLookupByLibrary.simpleMessage("تفاصيل الملاحظة"),
    "bookmarks" : MessageLookupByLibrary.simpleMessage("الفواصل المحفوظة"),
    "bookmark_title" : MessageLookupByLibrary.simpleMessage("إسم الفاصلة"),
    "add_new_bookmark" : MessageLookupByLibrary.simpleMessage("إضافة فاصلة"),
    "save" : MessageLookupByLibrary.simpleMessage("حفظ"),
    "edit" : MessageLookupByLibrary.simpleMessage("تعديل"),
    "azkar" : MessageLookupByLibrary.simpleMessage("حصن المسلم"),
    "qibla" : MessageLookupByLibrary.simpleMessage("إتجاه القبلة"),
    "salat" : MessageLookupByLibrary.simpleMessage("أوقات الصلاة"),
    "aya_count" : MessageLookupByLibrary.simpleMessage("عدد الآيات"),
    "quran_sorah" : MessageLookupByLibrary.simpleMessage("كل السور"),
    "about_us" : MessageLookupByLibrary.simpleMessage("عن التطبيق"),
    "stop_title" : MessageLookupByLibrary.simpleMessage("وقف القرآن الكريم - مكتبة الحكمة"),
    "about_app" : MessageLookupByLibrary.simpleMessage("يتميز تطبيق القرآن الكريم بإعتماده لطبعة مجمع الملك فهد لطباعة المصحف الشريف بالمدينة المنورة لما تتمتع به من موثوقية وإتقان.\nتطبيق القرآن الكريم برعاية من مكتبة الحكمة.\nوهو تطبيق متكامل المزايا يتيح لك القراءة التفاعلية مع نص المصحف الإلكتروني، والاستماع للتلاوات ودراسة القرآن الكريم وحفظه بكل سهولة ويسر."),
    "about_app3" : MessageLookupByLibrary.simpleMessage("◉ القراءة بالوضع العامودي والأفقي.\n◉ ييسر البرنامج خاصية البحث النصي في آيات القرآن من خلال البحث اللحظي وعرض النتائج مع الصفحات بالإضافة إلى إمكانية الذهاب للصفحة.\n◉ حفظ مواقع القراءة بحيث يتمكن القارئ من حفظ الصفحة والعودة لها متى شاء.\n◉ إضافة ملاحظات.\n◉ الإستماع للسور بقراءة الشيخين محمود خليل الحصري ، محمد صديق المنشاوي.\n◉ يتيح التطبيق التفسير لكل آية.\n◉ تغيير التفسير.\n◉ تغيير حجم خط التفسير.\n◉ فهرس للسور.\n◉ التنقل بين السّور بسهولة.\n◉ يتيح التطبيق قراءة أحكام التلاوة.\n◉ يتميز التطبيق بإضافة حصن المسلم كامل ومقسم بحسب الأذكار بحيث يمكن يسهل على القارئ التنقل بين الأقسام.\n◉ إضافة أي قسم إلى المفضلة.\n◉ يتيح البرنامج للقارئ القراءة الليلية والتي تلون الخلفية باللون الأسود والخطوط البيضاء لتعطي القارئ الراحة التامة عند القراءة في أجواء منخفضة الإضائة."),
    "about_app2" : MessageLookupByLibrary.simpleMessage("من اهم مميزات البرنامج :"),
    "email" : MessageLookupByLibrary.simpleMessage("حساب التواصل الخاص بالمكتبة\nalheekmahlib@gmail.com"),
    "select_player" : MessageLookupByLibrary.simpleMessage("إختر القارئ"),
    "delete" : MessageLookupByLibrary.simpleMessage("حذف"),
    "page" : MessageLookupByLibrary.simpleMessage("صفحة"),
    "search_word" : MessageLookupByLibrary.simpleMessage("الآية المراد البحث عنها"),
    "search_description" : MessageLookupByLibrary.simpleMessage("يمكنك البحث عن جميع آيات القرآن الكريم، فقط قم بكتابة كلمة من الآية."),
    "fontSize" : MessageLookupByLibrary.simpleMessage("تغيير حجم الخط"),
    "waqfName" : MessageLookupByLibrary.simpleMessage("علامات الوقف"),
    "onboardTitle1" : MessageLookupByLibrary.simpleMessage("واجهة سهلة"),
    "onboardDesc1" : MessageLookupByLibrary.simpleMessage("- سهولة البحث عن آية.\nتغيير اللغة.\nالإستماع للصفحة.\nتغيير القارء."),
    "onboardTitle2" : MessageLookupByLibrary.simpleMessage("إظهار التفسير"),
    "onboardDesc2" : MessageLookupByLibrary.simpleMessage("يمكن قراءة التفسير لكل آية عن طريق سحب القائمة إلى الأعلى."),
    "onboardTitle3" : MessageLookupByLibrary.simpleMessage("خيارات النقر"),
    "onboardDesc3" : MessageLookupByLibrary.simpleMessage("- عند النقر مرتين يتم تكبير الصفحة.\n2- عند النقر المطول يظهر لك خيار حفظ الصفحة.\n3- عند الضغط مرة واحدة تظهر لك القوائم."),
    "light" : MessageLookupByLibrary.simpleMessage("فاتح"),
    "dark" : MessageLookupByLibrary.simpleMessage("داكن"),
    "azkarfav" : MessageLookupByLibrary.simpleMessage("الأذكار المفضلة"),
    "themeTitle" : MessageLookupByLibrary.simpleMessage("القراءة الليلية"),
    "sorah" : MessageLookupByLibrary.simpleMessage("سورة"),
    "part" : MessageLookupByLibrary.simpleMessage("جزء"),
    "langChange" : MessageLookupByLibrary.simpleMessage("تغيير اللغة"),
    "tafChange" : MessageLookupByLibrary.simpleMessage('إختر التفسير'),
    "tafIbnkatheerN" : MessageLookupByLibrary.simpleMessage('تفسير ابن كثير'),
    "tafBaghawyN" : MessageLookupByLibrary.simpleMessage('تفسر البغوي'),
    "tafQurtubiN" : MessageLookupByLibrary.simpleMessage('تفسير القرطبي'),
    "tafSaadiN" : MessageLookupByLibrary.simpleMessage('تفسير السعدي'),
    "tafTabariN" : MessageLookupByLibrary.simpleMessage('تفسير الطبري'),
    "tafIbnkatheerD" : MessageLookupByLibrary.simpleMessage('تفسير القرآن العظيم'),
    "tafBaghawyD" : MessageLookupByLibrary.simpleMessage('معالم التنزيل'),
    "tafQurtubiD" : MessageLookupByLibrary.simpleMessage('الجامع لأحكام القرآن'),
    "tafSaadiD" : MessageLookupByLibrary.simpleMessage('تيسير الكريم الرحمن في تفسير كلام المنان'),
    "tafTabariD" : MessageLookupByLibrary.simpleMessage('جامع البيان في تفسير القرآن'),
    "appLang" : MessageLookupByLibrary.simpleMessage('لغة التطبيق'),
    "setting" : MessageLookupByLibrary.simpleMessage('الإعدادات'),
    "reader1" : MessageLookupByLibrary.simpleMessage('محمود خليل الحصري'),
    "reader2" : MessageLookupByLibrary.simpleMessage('محمد صديق المنشاوي'),
    "alheekmahlib" : MessageLookupByLibrary.simpleMessage('مكتبة الحكمة'),
    "backTo" : MessageLookupByLibrary.simpleMessage('رجوع للـ'),
    "next" : MessageLookupByLibrary.simpleMessage('التالي'),
    "start" : MessageLookupByLibrary.simpleMessage('أبدأ'),

  };
}
