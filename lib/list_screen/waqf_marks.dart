import 'package:alquranalkareem/custom_clipper/drawClip.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WaqfMarks extends StatefulWidget {
  @override
  _WaqfMarksState createState() => _WaqfMarksState();
}

class _WaqfMarksState extends State<WaqfMarks>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      value: 0.0,
      duration: Duration(seconds: 25),
      upperBound: 1,
      lowerBound: -1,
      vsync: this,
    )..repeat();
  }

  @override
  void dispose() {
    _controller.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var padding = MediaQuery.of(context).size;
    return Stack(
      children: <Widget>[
        AnimatedBuilder(
          animation: _controller,
          builder: (BuildContext context, Widget child) {
            return ClipPath(
              clipper: DrawClip(_controller.value),
              child: Container(
                height: size.height * 0.2,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.bottomLeft,
                      end: Alignment.topRight,
                      colors: [
                        Theme.of(context).primaryColor,
                        Theme.of(context).bottomAppBarColor
                      ]),
                ),
              ),
            );
          },
        ),
        Container(
          alignment: Alignment.topCenter,
          padding: EdgeInsets.only(top: 32),
          child: Image.asset(
            'assets/images/waqf.png',
            scale: 13.0,
            color: Theme.of(context).hoverColor,
          ),
        ),
        Stack(
          children: <Widget>[

            Padding(
                padding: const EdgeInsets.only(top: 16),
                child: ListView(
                  children: [
                    SizedBox(
                      height: padding.height *.2,
                    ),
                    Divider(
                      endIndent: 32,
                      indent: 32,
                      thickness: 2,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Row(
                        children: <Widget>[
                          Image.asset(
                            'assets/images/waqf_m.png',
                            scale: 14.0,
                            color: Theme.of(context).brightness ==
                                    Brightness.light
                                ? Theme.of(context).primaryColorLight
                                : Colors.white,
                          ),
                          Text(
                            ' - ',
                            style: TextStyle(
                              fontSize: 24,
                              color: Theme.of(context).brightness ==
                                      Brightness.light
                                  ? Theme.of(context).primaryColorLight
                                  : Colors.white,
                            ),
                          ),
                          Expanded(
                            child: Text(
                              'عَلَامَة الوَقْفِ اللَّازم نَحوُ : {إِنَّمَا يَسْتَجِيبُ الَّذِينَ يَسْمَعُونَ ۘ وَالْمَوْتَىٰ يَبْعَثُهُمُ اللَّهُ}.',
                              style: TextStyle(
                                fontSize: 22,
                                color: Theme.of(context).brightness ==
                                        Brightness.light
                                    ? Theme.of(context).primaryColorLight
                                    : Colors.white,
                              ),
                              textAlign: TextAlign.right,
                              maxLines: 5,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Divider(
                      height: 5,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Row(
                        children: <Widget>[
                          Image.asset(
                            'assets/images/waqf_g.png',
                            scale: 14.0,
                            color: Theme.of(context).brightness ==
                                    Brightness.light
                                ? Theme.of(context).primaryColorLight
                                : Colors.white,
                          ),
                          Text(
                            ' - ',
                            style: TextStyle(
                              fontSize: 24,
                              color: Theme.of(context).brightness ==
                                      Brightness.light
                                  ? Theme.of(context).primaryColorLight
                                  : Colors.white,
                            ),
                          ),
                          Expanded(
                            child: Text(
                              'عَلَامَة الوَقْفِ الجَائِزِ جَوَازًا مُسْتَوِيَ الطَّرَفَيْن نَحوُ : {نَّحْنُ نَقُصُّ عَلَيْكَ نَبَأَهُم بِالْحَقِّ ۚ إِنَّهُمْ فِتْيَةٌ آمَنُوا بِرَبِّهِمْ}.',
                              style: TextStyle(
                                fontSize: 22,
                                color: Theme.of(context).brightness ==
                                        Brightness.light
                                    ? Theme.of(context).primaryColorLight
                                    : Colors.white,
                              ),
                              textAlign: TextAlign.right,
                              maxLines: 5,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Divider(
                      height: 5,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Row(
                        children: <Widget>[
                          Image.asset(
                            'assets/images/waqf_s.png',
                            scale: 14.0,
                            color: Theme.of(context).brightness ==
                                    Brightness.light
                                ? Theme.of(context).primaryColorLight
                                : Colors.white,
                          ),
                          Text(
                            ' - ',
                            style: TextStyle(
                              fontSize: 24,
                              color: Theme.of(context).brightness ==
                                      Brightness.light
                                  ? Theme.of(context).primaryColorLight
                                  : Colors.white,
                            ),
                          ),
                          Expanded(
                            child: Text(
                              'عَلَامَة الوَقْفِ الجَائِزِ مَعَ كَوْنِ الوَصْل أَوْلَى نَحِوُ : {وَإِن يَمْسَسْكَ اللَّهُ بِضُرٍّ فَلَا كَاشِفَ لَهُ إِلَّا هُوَ ۖ وَإِن يَمْسَسْكَ بِخَيْرٍ فَهُوَ عَلَىٰ كُلِّ شَيْءٍ قَدِيرٌ}.',
                              style: TextStyle(
                                fontSize: 22,
                                color: Theme.of(context).brightness ==
                                        Brightness.light
                                    ? Theme.of(context).primaryColorLight
                                    : Colors.white,
                              ),
                              textAlign: TextAlign.right,
                              maxLines: 5,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Divider(
                      height: 5,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Row(
                        children: <Widget>[
                          Image.asset(
                            'assets/images/waqf_q.png',
                            scale: 14.0,
                            color: Theme.of(context).brightness ==
                                    Brightness.light
                                ? Theme.of(context).primaryColorLight
                                : Colors.white,
                          ),
                          Text(
                            ' - ',
                            style: TextStyle(
                              fontSize: 24,
                              color: Theme.of(context).brightness ==
                                      Brightness.light
                                  ? Theme.of(context).primaryColorLight
                                  : Colors.white,
                            ),
                          ),
                          Expanded(
                            child: Text(
                              'عَلَامَة الوَقْفِ الجَائِزِ مَعَ كَوْنِ الوَقْفِ أَوْلَى نَحِوُ : {قُل رَّبِّي أَعْلَمُ بِعِدَّتِهِم مَّا يَعْلَمُهُمْ إِلَّا قَلِيلٌ ۗ فَلَا تُمَارِ فِيهِمْ}.',
                              style: TextStyle(
                                fontSize: 22,
                                color: Theme.of(context).brightness ==
                                        Brightness.light
                                    ? Theme.of(context).primaryColorLight
                                    : Colors.white,
                              ),
                              textAlign: TextAlign.right,
                              maxLines: 5,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Divider(
                      height: 5,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Row(
                        children: <Widget>[
                          Image.asset(
                            'assets/images/waqf_noqat.png',
                            scale: 14.0,
                            color: Theme.of(context).brightness ==
                                    Brightness.light
                                ? Theme.of(context).primaryColorLight
                                : Colors.white,
                          ),
                          Text(
                            ' - ',
                            style: TextStyle(
                              fontSize: 24,
                              color: Theme.of(context).brightness ==
                                      Brightness.light
                                  ? Theme.of(context).primaryColorLight
                                  : Colors.white,
                            ),
                          ),
                          Expanded(
                            child: Text(
                              'عَلَامَةُ تَعَانُق الوَقْفِ بِحَيْثُ إِِذَا وَقِفَ عَلى أَحَدِ المَوْضِعَيْن لَا يَصِحُّ الوَقفُ عَلى الآخَرِ نَحِوُ : {ذَٰلِكَ الْكِتَابُ لَا رَيْبَ ۛ فِيهِ ۛ هُدًى لِّلْمُتَّقِينَ}.',
                              style: TextStyle(
                                fontSize: 22,
                                color: Theme.of(context).brightness ==
                                        Brightness.light
                                    ? Theme.of(context).primaryColorLight
                                    : Colors.white,
                              ),
                              textAlign: TextAlign.right,
                              maxLines: 5,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Divider(
                      thickness: 2,
                    ),
                  ],
                ))
          ],
        ),
      ],
    );
    //   Scaffold(
    //   backgroundColor: Theme.of(context).backgroundColor,
    //   body: Scrollbar(
    //       child: SingleChildScrollView(
    //         child: Column(
    //           children: <Widget>[
    //             Stack(
    //               children: [
    //                 AnimatedBuilder(
    //                   animation: _controller,
    //                   builder: (BuildContext context, Widget child) {
    //                     return ClipPath(
    //                       clipper: DrawClip(_controller.value),
    //                       child: Container(
    //                         height: size.height * 0.2,
    //                         decoration: BoxDecoration(
    //                           gradient: LinearGradient(
    //                               begin: Alignment.bottomLeft,
    //                               end: Alignment.topRight,
    //                               colors: [
    //                                 Theme.of(context).primaryColor,
    //                                 Theme.of(context).bottomAppBarColor
    //                               ]),
    //                         ),
    //                       ),
    //                     );
    //                   },
    //                 ),
    //                 Container(
    //                   alignment: Alignment.center,
    //                   padding: EdgeInsets.only(top: 32),
    //                   child: Image.asset(
    //                     'assets/images/waqf.png',
    //                     scale: 3.0,
    //                     color: Theme.of(context).hoverColor,
    //                   ),
    //                 ),
    //               ],
    //             ),
    //             Divider(
    //               endIndent: 32,
    //               indent: 32,
    //               thickness: 2,
    //             ),
    //             Padding(
    //               padding: const EdgeInsets.all(16.0),
    //               child: Row(
    //                 children: <Widget>[
    //                   Image.asset(
    //                     'assets/images/waqf_m.png',
    //                     scale: 14.0,
    //                     color: Theme.of(context).brightness == Brightness.light
    //                         ? Theme.of(context).primaryColorLight
    //                         : Colors.white,
    //                   ),
    //                   Text(
    //                     ' - ',
    //                     style: TextStyle(
    //                       fontSize: 24,
    //                       color: Theme.of(context).brightness == Brightness.light
    //                           ? Theme.of(context).primaryColorLight
    //                           : Colors.white,
    //                     ),
    //                   ),
    //                   Expanded(
    //                     child: Text(
    //                       'عَلَامَة الوَقْفِ اللَّازم نَحوُ : {إِنَّمَا يَسْتَجِيبُ الَّذِينَ يَسْمَعُونَ ۘ وَالْمَوْتَىٰ يَبْعَثُهُمُ اللَّهُ}.',
    //                       style: TextStyle(
    //                         fontSize: 22,
    //                         color: Theme.of(context).brightness == Brightness.light
    //                             ? Theme.of(context).primaryColorLight
    //                             : Colors.white,
    //                       ),
    //                       textAlign: TextAlign.right,
    //                       maxLines: 5,
    //                       overflow: TextOverflow.ellipsis,
    //                     ),
    //                   ),
    //                 ],
    //               ),
    //             ),
    //             Divider(
    //               height: 5,
    //             ),
    //             Padding(
    //               padding: const EdgeInsets.all(16.0),
    //               child: Row(
    //                 children: <Widget>[
    //                   Image.asset(
    //                     'assets/images/waqf_g.png',
    //                     scale: 14.0,
    //                     color: Theme.of(context).brightness == Brightness.light
    //                         ? Theme.of(context).primaryColorLight
    //                         : Colors.white,
    //                   ),
    //                   Text(
    //                     ' - ',
    //                     style: TextStyle(
    //                       fontSize: 24,
    //                       color: Theme.of(context).brightness == Brightness.light
    //                           ? Theme.of(context).primaryColorLight
    //                           : Colors.white,
    //                     ),
    //                   ),
    //                   Expanded(
    //                     child: Text(
    //                       'عَلَامَة الوَقْفِ الجَائِزِ جَوَازًا مُسْتَوِيَ الطَّرَفَيْن نَحوُ : {نَّحْنُ نَقُصُّ عَلَيْكَ نَبَأَهُم بِالْحَقِّ ۚ إِنَّهُمْ فِتْيَةٌ آمَنُوا بِرَبِّهِمْ}.',
    //                       style: TextStyle(
    //                         fontSize: 22,
    //                         color: Theme.of(context).brightness == Brightness.light
    //                             ? Theme.of(context).primaryColorLight
    //                             : Colors.white,
    //                       ),
    //                       textAlign: TextAlign.right,
    //                       maxLines: 5,
    //                       overflow: TextOverflow.ellipsis,
    //                     ),
    //                   ),
    //                 ],
    //               ),
    //             ),
    //             Divider(
    //               height: 5,
    //             ),
    //             Padding(
    //               padding: const EdgeInsets.all(16.0),
    //               child: Row(
    //                 children: <Widget>[
    //                   Image.asset(
    //                     'assets/images/waqf_s.png',
    //                     scale: 14.0,
    //                     color: Theme.of(context).brightness == Brightness.light
    //                         ? Theme.of(context).primaryColorLight
    //                         : Colors.white,
    //                   ),
    //                   Text(
    //                     ' - ',
    //                     style: TextStyle(
    //                       fontSize: 24,
    //                       color: Theme.of(context).brightness == Brightness.light
    //                           ? Theme.of(context).primaryColorLight
    //                           : Colors.white,
    //                     ),
    //                   ),
    //                   Expanded(
    //                     child: Text(
    //                       'عَلَامَة الوَقْفِ الجَائِزِ مَعَ كَوْنِ الوَصْل أَوْلَى نَحِوُ : {وَإِن يَمْسَسْكَ اللَّهُ بِضُرٍّ فَلَا كَاشِفَ لَهُ إِلَّا هُوَ ۖ وَإِن يَمْسَسْكَ بِخَيْرٍ فَهُوَ عَلَىٰ كُلِّ شَيْءٍ قَدِيرٌ}.',
    //                       style: TextStyle(
    //                         fontSize: 22,
    //                         color: Theme.of(context).brightness == Brightness.light
    //                             ? Theme.of(context).primaryColorLight
    //                             : Colors.white,
    //                       ),
    //                       textAlign: TextAlign.right,
    //                       maxLines: 5,
    //                       overflow: TextOverflow.ellipsis,
    //                     ),
    //                   ),
    //                 ],
    //               ),
    //             ),
    //             Divider(
    //               height: 5,
    //             ),
    //             Padding(
    //               padding: const EdgeInsets.all(16.0),
    //               child: Row(
    //                 children: <Widget>[
    //                   Image.asset(
    //                     'assets/images/waqf_q.png',
    //                     scale: 14.0,
    //                     color: Theme.of(context).brightness == Brightness.light
    //                         ? Theme.of(context).primaryColorLight
    //                         : Colors.white,
    //                   ),
    //                   Text(
    //                     ' - ',
    //                     style: TextStyle(
    //                       fontSize: 24,
    //                       color: Theme.of(context).brightness == Brightness.light
    //                           ? Theme.of(context).primaryColorLight
    //                           : Colors.white,
    //                     ),
    //                   ),
    //                   Expanded(
    //                     child: Text(
    //                       'عَلَامَة الوَقْفِ الجَائِزِ مَعَ كَوْنِ الوَقْفِ أَوْلَى نَحِوُ : {قُل رَّبِّي أَعْلَمُ بِعِدَّتِهِم مَّا يَعْلَمُهُمْ إِلَّا قَلِيلٌ ۗ فَلَا تُمَارِ فِيهِمْ}.',
    //                       style: TextStyle(
    //                         fontSize: 22,
    //                         color: Theme.of(context).brightness == Brightness.light
    //                             ? Theme.of(context).primaryColorLight
    //                             : Colors.white,
    //                       ),
    //                       textAlign: TextAlign.right,
    //                       maxLines: 5,
    //                       overflow: TextOverflow.ellipsis,
    //                     ),
    //                   ),
    //                 ],
    //               ),
    //             ),
    //             Divider(
    //               height: 5,
    //             ),
    //             Padding(
    //               padding: const EdgeInsets.all(16.0),
    //               child: Row(
    //                 children: <Widget>[
    //                   Image.asset(
    //                     'assets/images/waqf_noqat.png',
    //                     scale: 14.0,
    //                     color: Theme.of(context).brightness == Brightness.light
    //                         ? Theme.of(context).primaryColorLight
    //                         : Colors.white,
    //                   ),
    //                   Text(
    //                     ' - ',
    //                     style: TextStyle(
    //                       fontSize: 24,
    //                       color: Theme.of(context).brightness == Brightness.light
    //                           ? Theme.of(context).primaryColorLight
    //                           : Colors.white,
    //                     ),
    //                   ),
    //                   Expanded(
    //                     child: Text(
    //                       'عَلَامَةُ تَعَانُق الوَقْفِ بِحَيْثُ إِِذَا وَقِفَ عَلى أَحَدِ المَوْضِعَيْن لَا يَصِحُّ الوَقفُ عَلى الآخَرِ نَحِوُ : {ذَٰلِكَ الْكِتَابُ لَا رَيْبَ ۛ فِيهِ ۛ هُدًى لِّلْمُتَّقِينَ}.',
    //                       style: TextStyle(
    //                         fontSize: 22,
    //                         color: Theme.of(context).brightness == Brightness.light
    //                             ? Theme.of(context).primaryColorLight
    //                             : Colors.white,
    //                       ),
    //                       textAlign: TextAlign.right,
    //                       maxLines: 5,
    //                       overflow: TextOverflow.ellipsis,
    //                     ),
    //                   ),
    //                 ],
    //               ),
    //             ),
    //             Divider(
    //               thickness: 2,
    //             ),
    //           ],
    //         ),
    //       )),
    // );
  }
}
