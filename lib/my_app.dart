import 'package:alquranalkareem/list_screen/waqf_marks.dart';
import 'package:alquranalkareem/themes/custom_theme.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:preferences/preference_service.dart';
import 'package:provider/provider.dart';

import 'package:alquranalkareem/data/moor_database.dart';
import 'package:alquranalkareem/module/quran/show.dart';
import 'package:alquranalkareem/setting_page.dart';
import 'package:alquranalkareem/splash.dart';
import 'package:scoped_model/scoped_model.dart';

import 'athkar/athkar.dart';
import 'athkar/favourites.dart';
import 'helper/app_localizations.dart';
import 'helper/settings_helpers.dart';
import 'onboarding_screen.dart';

typedef void ChangeLocaleCallback(Locale locale);

class Application {
  static ChangeLocaleCallback changeLocale;
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  MyAppModel myAppModel;
  @override
  void initState() {
    myAppModel = MyAppModel(
      locale: Locale(
        'en',
        'ar',
      ),
    );

    Application.changeLocale = null;
    Application.changeLocale = changeLocale;

    var locale = SettingsHelpers.instance.getLocale();
    myAppModel.changeLocale(locale);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Color(0xff161f07)));
    return ScopedModel<MyAppModel>(
        model: myAppModel,
        child: ScopedModelDescendant<MyAppModel>(builder: (
          BuildContext context,
          Widget child,
          MyAppModel model,
        ) {
          return Provider(
              create: (_) => AppDatabase(),
              child: MaterialApp(
                builder: BotToastInit(), //1. call BotToastInit
                navigatorObservers: [BotToastNavigatorObserver()],
                debugShowCheckedModeBanner: false,
                title: "القران الكريم",
                localizationsDelegates: [
                  myAppModel.appLocalizationsDelegate,
                  GlobalMaterialLocalizations.delegate,
                  GlobalWidgetsLocalizations.delegate,
                ],
                supportedLocales: model.supportedLocales,
                locale: model.locale,
//            supportedLocales: [
//              Locale('ar', 'SA'),
//              Locale('en', 'US'),
//            ],
//            localizationsDelegates: [
//              AppLocalizations.delegate,
//              GlobalMaterialLocalizations.delegate,
//              GlobalWidgetsLocalizations.delegate,
//            ],
//             Returns a locale which will be used by the app
//            localeResolutionCallback: (locale, supportedLocales) {
//              for (var supportedLocale in supportedLocales) {
//                if (supportedLocale.languageCode ==
//                    PrefService.getString("lang")) {
//                  return supportedLocale;
//                }
//              }
////              // If the locale of the device is not supported, use the first one
////              // from the list (English, in this case).
//              return supportedLocales.first;
//            },
                theme: CustomTheme.of(context),
                home: SplashScreen(),
                routes: <String, WidgetBuilder>{
                  '/HomeScreen': (BuildContext context) => QuranShow(
                      initialPageNum: PrefService.getInt("start_page")),
                  '/SettingPage': (BuildContext context) => SettingPage(),
                  '/WaqfMarks': (BuildContext context) => WaqfMarks(),
                  '/athkar': (BuildContext context) => Athkar(),
                  '/favouriteList': (BuildContext context) => FavouriteList(),
                  '/onboardingScreen': (BuildContext context) =>
                      OnboardingScreen()
                },
              ),
            );
        }));
  }

  void changeLocale(Locale locale) {
    myAppModel.changeLocale(locale);
  }
}

class MyAppModel extends Model {
  AppLocalizationsDelegate appLocalizationsDelegate;
  Locale locale;

  List<Locale> supportedLocales = [
    Locale('en'),
    Locale('ar'),
  ];

  MyAppModel({
    @required this.locale,
  }) {
    appLocalizationsDelegate = AppLocalizationsDelegate(
      locale: locale,
      supportedLocales: supportedLocales,
    );
  }

  void changeLocale(Locale locale) {
    this.locale = locale;
    notifyListeners();
  }
}
