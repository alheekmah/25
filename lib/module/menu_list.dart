import 'package:alquranalkareem/athkar/athkar.dart';
import 'package:alquranalkareem/athkar/azkar_search.dart';
import 'package:alquranalkareem/athkar/favourites.dart';
import 'package:alquranalkareem/list_screen/menuPage.dart';
import 'package:alquranalkareem/list_screen/waqf_marks.dart';
import 'package:alquranalkareem/module/quran/quran_search.dart';
import 'package:alquranalkareem/module/quran/widgets/top_bar.dart';
import 'package:alquranalkareem/themes/custom_theme.dart';
import 'package:alquranalkareem/themes/themes.dart';
import 'package:animated_bottom_navigation_bar/animated_bottom_navigation_bar.dart';
import 'package:animations/animations.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MenuList extends StatefulWidget {
  @override
  _MenuListState createState() => _MenuListState();
}

class _MenuListState extends State<MenuList>
    with SingleTickerProviderStateMixin {
  void _changeTheme(BuildContext buildContext, MyThemeKeys key) {
    CustomTheme.instanceOf(buildContext).changeTheme(key);
  }

  var _bottomNavIndex = 0;
  AnimationController _animationController;
  Animation<double> animation;
  CurvedAnimation curve;

  final iconList = <IconData>[
    Icons.widgets,
    Icons.art_track,
    Icons.list,
    Icons.bookmark,
  ];

  final List<Widget> wid = <Widget>[
    MenuPage(),
    WaqfMarks(),
    Athkar(),
    FavouriteList(),
  ];

  @override
  void initState() {
    super.initState();

    _animationController = AnimationController(
      duration: Duration(seconds: 1),
      vsync: this,
    );
    curve = CurvedAnimation(
      parent: _animationController,
      curve: Interval(
        0.5,
        1.0,
        curve: Curves.fastOutSlowIn,
      ),
    );
    animation = Tween<double>(
      begin: 0,
      end: 1,
    ).animate(curve);

    Future.delayed(
      Duration(seconds: 1),
          () => _animationController.forward(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
          // floatingActionButton: ScaleTransition(
          //   scale: animation,
          //   child: FloatingActionButton(
          //     elevation: 8,
          //     backgroundColor: Theme.of(context).primaryColor,
          //     child: OpenContainer(
          //       closedColor: Theme.of(context).primaryColor,
          //       openColor: Theme.of(context).primaryColor,
          //       closedElevation: 2,
          //       openElevation: 8,
          //       transitionType: ContainerTransitionType.fade,
          //       transitionDuration: const Duration(milliseconds: 500),
          //       openBuilder: (context, action) {
          //         return PickerOpened(
          //               () => _animationController.forward(),
          //           showSearch(context: context, delegate: AzkarSearch(list: listmobilesearch)),
          //           Theme.of(context).primaryColor,
          //         );
          //       },
          //       closedBuilder: (context, action) {
          //         return PickerCollapsed(
          //             Theme.of(context).primaryColor,
          //             Icon(Icons.search,
          //               color: Colors.white,)
          //         );
          //       },
          //     ),
          //
          //     // Icon(
          //     //   Icons.search,
          //     //   color: Colors.white,
          //     // ),
          //     onPressed: () {
          //       _animationController.reset();
          //       _animationController.forward();
          //     },
          //   ),
          // ),
          // floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
          bottomNavigationBar: AnimatedBottomNavigationBar(
            icons: iconList,
            backgroundColor: Theme.of(context).bottomAppBarColor,
            activeIndex: _bottomNavIndex,
            activeColor: Theme.of(context).primaryColor,
            splashColor: Theme.of(context).primaryColorLight,
            inactiveColor: Colors.white,
            notchAndCornersAnimation: animation,
            splashSpeedInMilliseconds: 300,
            notchSmoothness: NotchSmoothness.defaultEdge,
            // gapLocation: GapLocation.end,
            leftCornerRadius: 0,
            rightCornerRadius: 0,
            onTap: (index) => setState(() => _bottomNavIndex = index),
          ),
          body: wid[_bottomNavIndex],
      ),
    );
  }
}
